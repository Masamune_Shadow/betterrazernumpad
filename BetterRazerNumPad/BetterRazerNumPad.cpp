
// BetterRazerNumPad.cpp
//

#pragma warning(push, 0)
#include "stdafx.h"
#pragma warning(pop)
#include "SwitchBlade.h"		// SwitchBlade SDK support
#include "BetterRazerNumpad.h"
#include "BetterRazerNumpadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// BetterRazerNumpadApp

BEGIN_MESSAGE_MAP(BetterRazerNumpadApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	ON_COMMAND_RANGE(IDC_BUTTON_DK1, IDC_BUTTON_DK10, &BetterRazerNumpadApp::OnBnClickedButtonDK)
END_MESSAGE_MAP()


// BetterRazerNumpadApp construction

BetterRazerNumpadApp::BetterRazerNumpadApp()
{
	// support Restart Manager
	m_dwRestartManagerSupportFlags = AFX_RESTART_MANAGER_SUPPORT_RESTART;

	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only BetterRazerNumpadApp object

BetterRazerNumpadApp theApp;


// BetterRazerNumpadApp initialization

BOOL BetterRazerNumpadApp::InitInstance()
{
	INT_PTR nResponse = 0;
	// InitCommonControlsEx() is required on Windows XP if an application
	// manifest specifies use of ComCtl32.dll version 6 or later to enable
	// visual styles.  Otherwise, any window creation will fail.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Set this to include all the common control classes you want to use
	// in your application.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

    CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	AfxEnableControlContainer();

	// Create the shell manager, in case the dialog contains
	// any shell tree view or shell list view controls.
	CShellManager *pShellManager = new CShellManager;

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	BetterRazerNumpadDlg dlg;
	dlg.m_RzSBStartStatus = S_OK;
	m_pMainWnd = &dlg;
	//
	// init the SwitchBlade SDK framework
	//

	dlg.m_ghInstance = AfxGetInstanceHandle();
	nResponse = dlg.DoModal();

	RzSBStop();
	RZSBSDK_EVENT_EXIT;
	// Delete the shell manager created above.
	if (pShellManager != NULL)
	{
		delete pShellManager;
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


//
// the idea here is to have the range of buttons all map to this function
// with an index instead of N calls which are the same.
//
void BetterRazerNumpadApp::OnBnClickedButtonDK(UINT nID)
{
	UINT index = nID - IDC_BUTTON_DK1;

	UNREFERENCED_PARAMETER(index);

}
