
// FunctionTest.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

// BetterRazerNumpadApp:
// See BetterRazerNumpadApp.cpp for the implementation of this class
//

class BetterRazerNumpadApp : public CWinApp
{
public:
	BetterRazerNumpadApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
	afx_msg void OnBnClickedButtonDK(UINT);
};

extern BetterRazerNumpadApp theApp;