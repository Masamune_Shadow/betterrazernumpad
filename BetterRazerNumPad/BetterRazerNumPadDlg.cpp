//TODO
/*
//seems to be some delay in the tactile keys 9which i've always had problems with
so look into possibly reducing the number of calls during a down press.
the blue and red are going to green when pressed (the function buttons) don't know why.

	(may not be possible due to current implimentation)
// enable transparancy/color wheel, so i'll be able to make tons of colors really easily/automatically, but that could require a whole restucturing
and possibly a remaking/using a form, which I'm not doing because that will cause lag, and that's not what i want.
*/
//////////////////////////////////////////////////////////////////////////////////////
// BetterRazerNumpad.cpp : implementation file
//////////////////////////////////////////////////////////////////////////////////////
#pragma warning(push, 0)
#include "stdafx.h"
#include "afxdialogex.h"
#pragma warning(pop)
#include "BetterRazerNumpad.h"
#include "BetterRazerNumpadDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//BEGIN_MESSAGE_MAP(CCamStreamerDlg, CDialogEx)
//	ON_WM_TIMER()
//END_MESSAGE_MAP()


extern LPCWSTR DKTYPEToString(_RZSBSDK_DKTYPE);                     // debugging support
extern LPCWSTR RZDKSTATEToString(_RZSBSDK_KEYSTATE);             // debugging support
extern LPCWSTR RZSBSDK_GESTURETYPEToString(RZSBSDK_GESTURETYPE);   // debugging support

HWND g_hwndDialog = NULL; // used to allow application termination event to send WM_CLOSE to hwnd

////////////////////////////////////////////////////////////////////////////////////////////////////
// Custom type used in this module only
////////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum _FILE_TYPE_ {
	ft_invalid = 0,
	ft_bmp,
	ft_gif,
	ft_jpg,
	ft_png,
	ft_undefined
} FILETYPE;

//////////////////////////////////////////////////////////////////////////////////////////////////
// A image file list for  rendering Up/Down of Dynamic Key
//////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _file_list_
{
	WCHAR up_filenames[RZSBSDK_DK_INVALID][MAX_STRING_LENGTH];
	WCHAR down_filenames[RZSBSDK_DK_INVALID][MAX_STRING_LENGTH];
	WCHAR numpadBG[MAX_STRING_LENGTH];
	WCHAR numpad2BG[MAX_STRING_LENGTH];
	WCHAR numpadFree[MAX_STRING_LENGTH];
} file_list;

file_list InitialDKImageFilenames = {
	{
		L"",
		L".\\images\\numlock_down.png",
		L".\\images\\divide.png",
		L".\\images\\multiply.png",
		L".\\images\\minus.png",
		L".\\images\\add.png",
		L".\\images\\blank.png",
		L".\\images\\blank.png",
		L".\\images\\blank.png",
		L".\\images\\calc.png",
		L".\\images\\freemove_down.png"
	},
	{
		L"",
		L".\\images\\numlock.png",
		L".\\images\\divide_down.png",
		L".\\images\\multiply_down.png",
		L".\\images\\minus_down.png",
		L".\\images\\add_down.png",
		L".\\images\\blank_down.png",
		L".\\images\\blank_down.png",
		L".\\images\\blank_down.png",
		L".\\images\\calc_down.png",
		L".\\images\\freemove.png"
	},
	{
		L".\\images\\numpad.png"
	},
	{
		L".\\images\\numpad2.png"
	},
	{
		L".\\images\\freepress.png"
	}
};


bool FreePress = false; // means that the user can use this as a regular mousepad
bool ISNUMLOCKON = true; //perhaps get value from system?
bool Sent = false;
bool numpadTimerSent = false;

bool dynamicSent = false;
bool dynamicTimerSent = false;

bool DynamicKeyTimerActive = false;
bool NumPadTimerActive = false;
bool DynamicKeyTimerFirstTime = true;
bool NumPadTimerFirstTime = true;
int iDynamicKeyTimer;
int iNumPadTimer;
HWND windowHandle = NULL;
DWORD processID = NULL;
bool fCalcOpen = false;
PROCESS_INFORMATION ProcessInfo; //This is what we get as an [out] parameter
					STARTUPINFO StartupInfo; //This is an [in] parameter


struct DynamicKey{
	_RZSBSDK_DKTYPE Key;
	_RZSBSDK_KEYSTATE CurrentState;
	_RZSBSDK_KEYSTATE PreviousState;
	bool fFirstPass;
	bool fPressPass;
	int VirtualKey;
	};
DynamicKey DK[10];

struct TrackPad{
	bool fPressPass;
	bool fFirstPass;
	int VirtualKey;
};
TrackPad TP[13];

int dynamicLastPressed = 0;
int trackpadLastPressed = 9999;
int trackpadPressed = 9999;

DWORD previousDWParameter = 0;
_RZSBSDK_KEYSTATE previousDkState = RZSBSDK_KEYSTATE_UP;

////////////////////////////////////////////////////////////////////
// Adds a string to the output box, and scrolls to it if neccessary
///////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////
// calls this function after each interval specified in the SetTimer
//////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::OnTimer(UINT_PTR nIDEvent)
{
	WCHAR filename[MAX_STRING_LENGTH] = L"";
	//TriggerTimer(nIDEvent);
	//--> which will then possibly do TriggerResetTimer(UINT_PTR parent, int Case)
	//get through the "special" cases
	switch (nIDEvent){
		case 1:	//NUM LOCK
			if (DK[0].CurrentState == RZSBSDK_KEYSTATE_DOWN) //only activate on down
			{				
				if (DK[0].fFirstPass)
				{
					DK[0].fFirstPass = false;
					ISNUMLOCKON = ! ISNUMLOCKON;
					keybd_event(VK_NUMLOCK, 0, 0, 0);
					if (ISNUMLOCKON){
						RzSBSetImageDynamicKey(RZSBSDK_DK_1,RZSBSDK_KEYSTATE_UP,InitialDKImageFilenames.up_filenames[1]);
						RzSBSetImageTouchpad(InitialDKImageFilenames.numpadBG);//this will need to be something else, included in the project prob.
					}	else /*NOT ON*/	{
						RzSBSetImageDynamicKey(RZSBSDK_DK_1,RZSBSDK_KEYSTATE_UP,InitialDKImageFilenames.down_filenames[1]);
						RzSBSetImageTouchpad(InitialDKImageFilenames.numpad2BG);//this will need to be something else, included in the project prob.
					}
					SetAllTrackPadKeys();
				}
			}
			else
			{
				DK[0].fFirstPass = true;
			}
			//display the numlock enabled button
			//if numlock is enabled, all buttons act normalls
			//if disabled, then have it do the others WHICH ARE
			break;
		
			case 9:
			if (DK[8].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
			{					
				if (DK[8].fFirstPass)
				{
					DK[8].fFirstPass = false;
					ZeroMemory(&StartupInfo, sizeof(StartupInfo));
					StartupInfo.cb = sizeof StartupInfo ; //Only compulsory field

					if (fCalcOpen)
					{
						
						TerminateProcess(ProcessInfo.hProcess,PROCESS_TERMINATE);	
						CloseHandle(ProcessInfo.hThread);
						CloseHandle(ProcessInfo.hProcess);
						fCalcOpen = false;
						break;
					}
					else
					{
						if(CreateProcess(L"c:\\windows\\System32\\calc.exe", NULL, 
							NULL,NULL,FALSE,0,NULL,
							NULL,&StartupInfo,&ProcessInfo))
						{ 
							fCalcOpen = true;
						}  
					}
				}
			}
			else
			{
				DK[8].fFirstPass = true;
			}
			break;
		case 10:
			//should free press be only usable when it's pressed d
			//should it be toggled or held down to be used?
			//if (DK[9].CurrentState == RZSBSDK_KEYSTATE_DOWN && DK[9].CurrentState != DK[9].PreviousState)
			if (DK[9].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
			{
				if (DK[9].fFirstPass)
				{
					DK[9].fFirstPass = false;
					FreePress = !FreePress;
					if (FreePress){
						RzSBSetImageDynamicKey(RZSBSDK_DK_10,RZSBSDK_KEYSTATE_UP,InitialDKImageFilenames.down_filenames[10]);
						RzSBSetImageTouchpad(InitialDKImageFilenames.numpadFree);//this will need to be something else, included in the project prob.
					}	
					else
					{
						RzSBSetImageDynamicKey(RZSBSDK_DK_10,RZSBSDK_KEYSTATE_UP,InitialDKImageFilenames.up_filenames[10]);
						if (ISNUMLOCKON)
						{
							RzSBSetImageTouchpad(InitialDKImageFilenames.numpadBG);//this will need to be something else, included in the project prob.
						}
						else
						{
							RzSBSetImageTouchpad(InitialDKImageFilenames.numpad2BG);//this will need to be something else, included in the project prob.
						}
					}

					SetFreePress();
				}
			}
			else
			{
				DK[9].fFirstPass = true;
			}
			break;



		case 2:	///
				if (DK[1].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
				{
//					RzSBSetImageDynamicKey(RZSBSDK_DK_2,RZSBSDK_KEYSTATE_DOWN,InitialDKImageFilenames.up_filenames[1]);
					if (!DK[1].fPressPass)
					{
						if (DK[1].fFirstPass)
						{
							keybd_event(VK_DIVIDE, 0, 0, 0);
							DK[1].fFirstPass = false;
						}
						else
						{
							DK[1].fFirstPass = true;
							KillTimer(2);
							SetTimer(2222,25,NULL);
						}
					}
					else
					{
						keybd_event(VK_DIVIDE, 0, 0, 0);
						DK[1].fPressPass = false;
						KillTimer(2);
						SetTimer(2,150,NULL);
					}
				}
				else
				{
					DK[1].fFirstPass = true;
					DK[1].fPressPass = true;
					KillTimer(2);
					SetTimer(2,1,NULL);
				}
			break;
		case 3://*
				if (DK[2].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
				{
					if (!DK[2].fPressPass)
					{
						if (DK[2].fFirstPass)
						{
							keybd_event(VK_MULTIPLY, 0, 0, 0);
							DK[2].fFirstPass = false;
						}
						else
						{
							DK[2].fFirstPass = true;
							KillTimer(3);
							SetTimer(3333,25,NULL);
						}
					}
					else
					{
						keybd_event(VK_MULTIPLY, 0, 0, 0);
						DK[2].fPressPass = false;
						KillTimer(3);
						SetTimer(3,150,NULL);
					}
				}
				else
				{
					DK[2].fFirstPass = true;
					DK[2].fPressPass = true;
					KillTimer(3);
					SetTimer(3,1,NULL);
				}
			break;
		case 4://-
			if (DK[3].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
				{
					if (!DK[3].fPressPass)
					{
						if (DK[3].fFirstPass)
						{
							keybd_event(VK_SUBTRACT, 0, 0, 0);
							DK[3].fFirstPass = false;
						}
						else
						{
							DK[3].fFirstPass = true;
							KillTimer(4);
							SetTimer(4444,25,NULL);
						}
					}
					else
					{
						keybd_event(VK_SUBTRACT, 0, 0, 0);
						DK[3].fPressPass = false;
						KillTimer(4);
						SetTimer(4,150,NULL);
					}
				}
				else
				{
					DK[3].fFirstPass = true;
					DK[3].fPressPass = true;
					KillTimer(4);
					SetTimer(4,1,NULL);
				}
			break;
		case 5://+
			if (DK[4].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
				{
					if (!DK[4].fPressPass)
					{
						if (DK[4].fFirstPass)
						{
							keybd_event(VK_ADD, 0, 0, 0);
							DK[4].fFirstPass = false;
						}
						else
						{
							DK[4].fFirstPass = true;
							KillTimer(5);
							SetTimer(5555,25,NULL);
						}
					}
					else
					{
						keybd_event(VK_ADD, 0, 0, 0);
						DK[4].fPressPass = false;
						KillTimer(5);
						SetTimer(5,150,NULL);
					}
				}
				else
				{
					DK[4].fFirstPass = true;
					DK[4].fPressPass = true;
					KillTimer(5);
					SetTimer(5,1,NULL);
				}
			break;
		case 6:
			//if (DK[5].CurrentState ==RZSBSDK_KEYSTATE_DOWN)
			//{					
			//	if (DK[5].fFirstPass)
			//	{
			//		DK[5].fFirstPass = false;
			//		//bring up the color change
			//	}
			//}
			//else
			//{
			//	DK[5].fFirstPass = true;
			//}
			//break;

		

	//NUMPAD LOGIC


			case 1000:
				if (trackpadPressed == 1000)
				{
					if (!TP[0].fPressPass)
					{
						if (TP[0].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD1, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_END, 0, 0, 0);
							}
							TP[0].fFirstPass = false;
						}
						else
						{
							TP[0].fFirstPass = true;
							KillTimer(1000);
							SetTimer(1001,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD1, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_END, 0, 0, 0);
							}
						TP[0].fPressPass = false;
						KillTimer(1000);
						SetTimer(1000,150,NULL);
					}
				}
				else
				{
					TP[0].fFirstPass = true;
					TP[0].fPressPass = true;
					KillTimer(1000);
					SetTimer(1000,1,NULL);
				}
			break;

			case 1001:	///
				if (trackpadPressed == 1000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD1, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_END, 0, 0, 0);
							}
				}
				else
				{
					TP[0].fFirstPass = true;
					KillTimer(1001);
					SetTimer(1000,1,NULL);
				}
				break;

			case 2000:
				if (trackpadPressed == 2000)
				{
					if (!TP[1].fPressPass)
					{
						if (TP[1].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD2, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DOWN, 0, 0, 0);
							}
							TP[1].fFirstPass = false;
						}
						else
						{
							TP[1].fFirstPass = true;
							KillTimer(2000);
							SetTimer(2002,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD2, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DOWN, 0, 0, 0);
							}
						TP[1].fPressPass = false;
						KillTimer(2000);
						SetTimer(2000,150,NULL);
					}
				}
				else
				{
					TP[1].fFirstPass = true;
					TP[1].fPressPass = true;
					KillTimer(2000);
					SetTimer(2000,1,NULL);
				}
			break;

			case 2002:	///
				if (trackpadPressed == 2000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD2, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DOWN, 0, 0, 0);
							}
				}
				else
				{
					TP[1].fFirstPass = true;
					KillTimer(2002);
					SetTimer(2000,1,NULL);
				}
				break;


case 3000:
				if (trackpadPressed == 3000)
				{
					if (!TP[2].fPressPass)
					{
						if (TP[2].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD3, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_NEXT, 0, 0, 0);
							}
							TP[2].fFirstPass = false;
						}
						else
						{
							TP[2].fFirstPass = true;
							KillTimer(3000);
							SetTimer(3003,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD3, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_NEXT, 0, 0, 0);
							}
						TP[2].fPressPass = false;
						KillTimer(3000);
						SetTimer(3000,150,NULL);
					}
				}
				else
				{
					TP[2].fFirstPass = true;
					TP[2].fPressPass = true;
					KillTimer(3000);
					SetTimer(3000,1,NULL);
				}
			break;

			case 3003:	///
				if (trackpadPressed == 3000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD3, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_NEXT, 0, 0, 0);
							}
				}
				else
				{
					TP[2].fFirstPass = true;
					KillTimer(3003);
					SetTimer(3000,1,NULL);
				}
				break;




			 
			case 4000:
				if (trackpadPressed == 4000)
				{
					if (!TP[3].fPressPass)
					{
						if (TP[3].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD4, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_LEFT, 0, 0, 0);
							}
							TP[3].fFirstPass = false;
						}
						else
						{
							TP[3].fFirstPass = true;
							KillTimer(4000);
							SetTimer(4004,25,NULL);
						}
					}
					else
					{
						if (ISNUMLOCKON)
						{
							keybd_event(VK_NUMPAD4, 0, 0, 0);
						}
						else
						{
							keybd_event(VK_LEFT, 0, 0, 0);
						}
						TP[3].fPressPass = false;
						KillTimer(4000);
						SetTimer(4000,150,NULL);
					}
				}
				else
				{
					TP[3].fFirstPass = true;
					TP[3].fPressPass = true;
					KillTimer(4000);
					SetTimer(4000,1,NULL);
				}
			break;

			case 4004:	///
				if (trackpadPressed == 4000)
				{
					if (ISNUMLOCKON)
					{
						keybd_event(VK_NUMPAD4, 0, 0, 0);
					}
					else
					{
						keybd_event(VK_LEFT, 0, 0, 0);
					}
				}
				else
				{
					TP[3].fFirstPass = true;
					KillTimer(4004);
					SetTimer(4000,1,NULL);
				}
				break;

case 5000:
				if (trackpadPressed == 5000)
				{
					if (!TP[4].fPressPass)
					{
						if (TP[4].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD5, 0, 0, 0);
							}
							else
							{
								//keybd_event(VK_LEFT, 0, 0, 0);
							}
							TP[4].fFirstPass = false;
						}
						else
						{
							TP[4].fFirstPass = true;
							KillTimer(5000);
							SetTimer(5005,25,NULL);
						}
					}
					else
					{
						if (ISNUMLOCKON)
						{
							keybd_event(VK_NUMPAD5, 0, 0, 0);
						}
						else
						{
							//keybd_event(VK_LEFT, 0, 0, 0);
						}
						TP[4].fPressPass = false;
						KillTimer(5000);
						SetTimer(5000,150,NULL);
					}
				}
				else
				{
					TP[4].fFirstPass = true;
					TP[4].fPressPass = true;
					KillTimer(5000);
					SetTimer(5000,1,NULL);
				}
			break;

			case 5005:	///
				if (trackpadPressed == 5000)
				{
					if (ISNUMLOCKON)
					{
						keybd_event(VK_NUMPAD5, 0, 0, 0);
					}
				}
				else
				{
					TP[4].fFirstPass = true;
					KillTimer(5005);
					SetTimer(5000,1,NULL);
				}
				break;

case 6000:
				if (trackpadPressed == 6000)
				{
					if (!TP[5].fPressPass)
					{
						if (TP[5].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD6, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_RIGHT, 0, 0, 0);
							}
							TP[5].fFirstPass = false;
						}
						else
						{
							TP[5].fFirstPass = true;
							KillTimer(6000);
							SetTimer(6006,25,NULL);
						}
					}
					else
					{
						if (ISNUMLOCKON)
						{
							keybd_event(VK_NUMPAD6, 0, 0, 0);
						}
						else
						{
							keybd_event(VK_RIGHT, 0, 0, 0);
						}
						TP[5].fPressPass = false;
						KillTimer(6000);
						SetTimer(6000,150,NULL);
					}
				}
				else
				{
					TP[5].fFirstPass = true;
					TP[5].fPressPass = true;
					KillTimer(6000);
					SetTimer(6000,1,NULL);
				}
			break;

			case 6006:	///
				if (trackpadPressed == 6000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD6, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_RIGHT, 0, 0, 0);
							}
				}
				else
				{
					TP[5].fFirstPass = true;
					KillTimer(6006);
					SetTimer(6000,1,NULL);
				}
				break;

			case 7000:
				if (trackpadPressed == 7000)
				{
					if (!TP[6].fPressPass)
					{
						if (TP[6].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD7, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_HOME, 0, 0, 0);
							}
							TP[6].fFirstPass = false;
						}
						else
						{
							TP[6].fFirstPass = true;
							KillTimer(7000);
							SetTimer(7007,25,NULL);
						}
					}
					else
					{
						if (ISNUMLOCKON)
						{
							keybd_event(VK_NUMPAD7, 0, 0, 0);
						}
						else
						{
							keybd_event(VK_HOME, 0, 0, 0);
						}
						TP[6].fPressPass = false;
						KillTimer(7000);
						SetTimer(7000,150,NULL);
					}
				}
				else
				{
					TP[6].fFirstPass = true;
					TP[6].fPressPass = true;
					KillTimer(7000);
					SetTimer(7000,1,NULL);
				}
			break;

			case 7007:	///
				if (trackpadPressed == 7000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD7, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_HOME, 0, 0, 0);
							}
				}
				else
				{
					TP[6].fFirstPass = true;
					KillTimer(7007);
					SetTimer(7000,1,NULL);
				}
				break;

			case 8000:
				if (trackpadPressed == 8000)
				{
					if (!TP[7].fPressPass)
					{
						if (TP[7].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD8, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_UP, 0, 0, 0);
							}
							TP[7].fFirstPass = false;
						}
						else
						{
							TP[7].fFirstPass = true;
							KillTimer(8000);
							SetTimer(8008,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD8, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_UP, 0, 0, 0);
							}
						TP[7].fPressPass = false;
						KillTimer(8000);
						SetTimer(8000,150,NULL);
					}
				}
				else
				{
					TP[7].fFirstPass = true;
					TP[7].fPressPass = true;
					KillTimer(8000);
					SetTimer(8000,1,NULL);
				}
			break;

			case 8008:	///
				if (trackpadPressed == 8000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD8, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_UP, 0, 0, 0);
							}
				}
				else
				{
					TP[7].fFirstPass = true;
					KillTimer(8008);
					SetTimer(8000,1,NULL);
				}
				break;

			 case 9000:
				if (trackpadPressed == 9000)
				{
					if (!TP[8].fPressPass)
					{
						if (TP[8].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD9, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_PRIOR, 0, 0, 0);
							}
							TP[8].fFirstPass = false;
						}
						else
						{
							TP[8].fFirstPass = true;
							KillTimer(9000);
							SetTimer(9009,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD9, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_PRIOR, 0, 0, 0);
							}
						TP[8].fPressPass = false;
						KillTimer(9000);
						SetTimer(9000,150,NULL);
					}
				}
				else
				{
					TP[8].fFirstPass = true;
					TP[8].fPressPass = true;
					KillTimer(9000);
					SetTimer(9000,1,NULL);
				}
			break;

			case 9009:	///
				if (trackpadPressed == 9000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD9, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_PRIOR, 0, 0, 0);
							}
				}
				else
				{
					TP[8].fFirstPass = true;
					KillTimer(9009);
					SetTimer(9000,1,NULL);
				}
				break;

			case 0000:
				if (trackpadPressed == 0000)
				{
					if (!TP[9].fPressPass)
					{
						if (TP[9].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD0, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_INSERT, 0, 0, 0);
							}
							TP[9].fFirstPass = false;
						}
						else
						{
							TP[9].fFirstPass = true;
							KillTimer(0000);
							SetTimer(90000,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD0, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_INSERT, 0, 0, 0);
							}
						TP[9].fPressPass = false;
						KillTimer(0000);
						SetTimer(0000,150,NULL);
					}
				}
				else
				{
					TP[9].fFirstPass = true;
					TP[9].fPressPass = true;
					KillTimer(0000);
					SetTimer(0000,1,NULL);
				}
			break;

			case 90000:	///
				if (trackpadPressed == 0000)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_NUMPAD0, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_INSERT, 0, 0, 0);
							}
				}
				else
				{
					TP[9].fFirstPass = true;
					KillTimer(90000);
					SetTimer(0000,1,NULL);
				}
				break;

			 case 1337:
				if (trackpadPressed == 1337)
				{
					if (!TP[10].fPressPass)
					{
						if (TP[10].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
							TP[10].fFirstPass = false;
						}
						else
						{
							TP[10].fFirstPass = true;
							KillTimer(1337);
							SetTimer(91337,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
						TP[10].fPressPass = false;
						KillTimer(1337);
						SetTimer(1337,150,NULL);
					}
				}
				else
				{
					TP[10].fFirstPass = true;
					TP[10].fPressPass = true;
					KillTimer(1337);
					SetTimer(1337,1,NULL);
				}
			break;

			case 91337:	///
				if (trackpadPressed == 1337)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_RETURN, 0, 0, 0);
							}
				}
				else
				{
					TP[10].fFirstPass = true;
					KillTimer(1337);
					SetTimer(1337,1,NULL);
				}
				break;

			 case 7770:
				if (trackpadPressed == 7770)
				{
					if (!TP[11].fPressPass)
					{
						if (TP[11].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_DECIMAL, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DELETE, 0, 0, 0);
							}
							TP[11].fFirstPass = false;
						}
						else
						{
							TP[11].fFirstPass = true;
							KillTimer(7770);
							SetTimer(97770,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_DECIMAL, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DELETE, 0, 0, 0);
							}
						TP[11].fPressPass = false;
						KillTimer(7770);
						SetTimer(7770,150,NULL);
					}
				}
				else
				{
					TP[11].fFirstPass = true;
					TP[11].fPressPass = true;
					KillTimer(7770);
					SetTimer(7770,1,NULL);
				}
			break;

			case 97770:	///
				if (trackpadPressed == 7770)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_DECIMAL, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_DELETE, 0, 0, 0);
							}
				}
				else
				{
					TP[11].fFirstPass = true;
					KillTimer(7770);
					SetTimer(7770,1,NULL);
				}
				break;

case 4545:
				if (trackpadPressed == 4545)
				{
					if (!TP[12].fPressPass)
					{
						if (TP[12].fFirstPass)
						{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
							TP[12].fFirstPass = false;
						}
						else
						{
							TP[12].fFirstPass = true;
							KillTimer(4545);
							SetTimer(94545,25,NULL);
						}
					}
					else
					{
													if (ISNUMLOCKON)
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
						TP[12].fPressPass = false;
						KillTimer(4545);
						SetTimer(4545,150,NULL);
					}
				}
				else
				{
					TP[12].fFirstPass = true;
					TP[12].fPressPass = true;
					KillTimer(4545);
					SetTimer(4545,1,NULL);
				}
			break;

			case 94545:	///
				if (trackpadPressed == 4545)
				{
							if (ISNUMLOCKON)
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
							else
							{
								keybd_event(VK_BACK, 0, 0, 0);
							}
				}
				else
				{
					TP[12].fFirstPass = true;
					KillTimer(4545);
					SetTimer(4545,1,NULL);
				}
				break;

		}
	
}
	

//////////////////////////////////////////////////////////////
// BetterRazerNumpadDlg dialog
//////////////////////////////////////////////////////////////
BetterRazerNumpadDlg::BetterRazerNumpadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(BetterRazerNumpadDlg::IDD, pParent),
    m_hwndDialog(NULL)
{
	//m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hwndDialog = CDialogEx::CDialog::CWnd::m_hWnd;
	m_hWnd = (HWND)*pParent;
}


void BetterRazerNumpadDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(BetterRazerNumpadDlg, CDialogEx)
    ON_WM_TIMER()
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////////
// App Event Callback  -- fires when the AppManager wants us to quit
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE MyAppEventCallback(
    _RZSBSDK_EVENTTYPE rzEvent,
	DWORD dwAppMode,
	DWORD dwProcessID
	)
{
	HRESULT hr = S_OK;

    hr = BetterRazerNumpadDlg::ApplicationEventCallback(rzEvent, dwAppMode, dwProcessID);

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display gesture message in the current dialog box
// when MyGestureCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT BetterRazerNumpadDlg::ApplicationEventCallback(
    RZSBSDK_EVENTTYPETYPE rzEvent,
	DWORD dwAppMode,
	DWORD dwProcessID
	)
{
	HRESULT hr = S_OK;

	if (RZSBSDK_EVENT_DEACTIVATED == rzEvent || RZSBSDK_EVENT_CLOSE == rzEvent)
	{
		PostQuitMessage(0);
	}

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Local dynamickey callback  used in SBSDK--RzSBDynamicKeySetCallback
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE
MyDynamicKeyCallback(_RZSBSDK_DKTYPE dk, _RZSBSDK_KEYSTATE dkState)
{
	HRESULT hr = S_OK;
	//if (dkState != previousDkState){
		hr = BetterRazerNumpadDlg::DynamicKeyEventCallback(dk, dkState);	

	//}
	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display DynamicKey message in the current dialog box
// when MyDynamicKeyCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT BetterRazerNumpadDlg::DynamicKeyEventCallback(RZSBSDK_DKTYPE dk, RZSBSDK_KEYSTATETYPE dkState)
{
	HRESULT hr = S_OK;
	//WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int Send = 0;
	switch (dk){
		case RZSBSDK_DK_1:	//NUM LOCK
			DK[0].CurrentState = dkState;
			Send = 1111;
			break;
		case RZSBSDK_DK_2:	///
			DK[1].CurrentState = dkState;
			Send = 2222;
			break;
		case RZSBSDK_DK_3://*
			DK[2].CurrentState = dkState;
			Send = 3333;
			break;
		case RZSBSDK_DK_4://-
			DK[3].CurrentState = dkState;
			Send = 4444;
			break;
		case RZSBSDK_DK_5://+
			DK[4].CurrentState = dkState;
			Send = 5555;
			break;
		case RZSBSDK_DK_6:
			DK[5].CurrentState = dkState;
			Send = 6666;
			break;
		case RZSBSDK_DK_7:
			DK[6].CurrentState = dkState;
			Send = 7777;
			break;
		case RZSBSDK_DK_8:
			DK[7].CurrentState = dkState;
			Send = 8888;
			break;
		case RZSBSDK_DK_9: //calc launch
			DK[8].CurrentState = dkState;
			Send = 9999;
			break;
		case RZSBSDK_DK_10: //free movement
			DK[9].CurrentState = dkState;
			Send = 1010;
			break;
	}
	
	

	return hr;
}

//////////////////////////////////////////////////////////////////////////////
// Local gesture callback  used in SBSDK--RzSBGestureSetCallback
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE MyGestureCallback(
    RZSBSDK_GESTURETYPE gesture,
	DWORD dwParameters,
	WORD wXPos,
	WORD wYPos,
	WORD wZPos
	)
{
	HRESULT hr = S_OK;
	/*RZSBSDK_GESTURE_INVALID	0x00000000
#define	RZSBSDK_GESTURE_NONE      0x00000001
#define RZSBSDK_GESTURE_PRESS     0x00000002
#define RZSBSDK_GESTURE_TAP       0x00000004
#define RZSBSDK_GESTURE_FLICK     0x00000008
#define RZSBSDK_GESTURE_ZOOM      0x00000010
#define RZSBSDK_GESTURE_ROTATE    0x00000020
#define RZSBSDK_GESTURE_ALL       0x0000003e
#define	RZSBSDK_GESTURE_UNDEFINED 0xffffffc0*/
	/*if (ISNUMLOC){
		if (gesture == 0x00000002){*/
	if (!FreePress){
			hr = BetterRazerNumpadDlg::GestureEventCallback(gesture,dwParameters,wXPos,wYPos,wZPos);
	}
	//	}
	//}

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display gesture message in the current dialog box
// when MyGestureCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT
BetterRazerNumpadDlg::GestureEventCallback(
    RZSBSDK_GESTURETYPE gesture,
	DWORD dwParameters,
	WORD wXPos,
	WORD wYPos,
	WORD wZPos
	)
{
	HRESULT hr = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	WCHAR szFlickDirections[5][20] = { L"unspecified", L"left", L"right", L"up", L"down" };


	bool XValid, YValid;
	XValid = false;
	YValid = false;
	int Send = 9999;
	
	switch (gesture) {
	//case RZSBSDK_GESTURE_INVALID:
	//	wsprintf(outstring, L"RZSBSDK_GESTURE_INVALID received!");
	//	break;
	case RZSBSDK_GESTURE_NONE:
		Send = 9999;
		break;
		
		//what about if tap(parameters = down), then start repeat timer until ANY tap(up) is pressed?
		//or, yeah, 
		//if tap_down, init that # and start repeat timer
		//then start the press which will determine the changed SEND variable (if at all)xxx
		//
		//exit on tap_up //tap only occurs on UP

		//capture the inputs over .01 ms?, if it registers as an up? 
	case RZSBSDK_GESTURE_PRESS:
		//wsprintf(outstring, L"Press(%ws,%d,%d)", (dwParameters == 0) ? L"Up" : L"Down", wXPos, wYPos);

		if (wXPos >= 176 && wXPos <= 330 && wYPos >= 335 && wYPos <= 480){//1
			Send = 1000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 335 && wYPos <= 480){//2
			Send = 2000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 335 && wYPos <= 480)	{//3
			Send = 3000;
		}	else if (wXPos >= 176 && wXPos <= 330 && wYPos >= 150 && wYPos <= 300){//4
			Send = 4000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 150 && wYPos <= 300){//5
			Send = 5000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 150 && wYPos <= 300){//6
			Send = 6000;
		}	else if (wXPos >= 176 && wXPos <= 330 && wYPos >= 0 && wYPos <= 150){//7
			Send = 7000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 0 && wYPos <= 150){//8
			Send = 8000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 0 && wYPos <= 150){//9
			Send = 9000;
		}	else if (wXPos >= 0 && wXPos <= 160 && wYPos >= 280 && wYPos <= 480){//0
			Send = 0000;
		}	else if (wXPos >= 0 && wXPos <= 160 && wYPos >= 0 && wYPos <= 220){//.
			Send = 7770;
		}	else if (wXPos >= 660 && wXPos <= 800 && wYPos >= 280 && wYPos <= 480){//ENTER
			Send = 1337;
		}	else if (wXPos >= 660 && wXPos <= 800 && wYPos >= 0 && wYPos <= 220){//Backspace
			Send = 4545;
		}
		break;
	case RZSBSDK_GESTURE_MOVE:
		if (wXPos >= 176 && wXPos <= 330 && wYPos >= 335 && wYPos <= 480){//1
			Send = 1000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 335 && wYPos <= 480){//2
			Send = 2000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 335 && wYPos <= 480)	{//3
			Send = 3000;
		}	else if (wXPos >= 176 && wXPos <= 330 && wYPos >= 150 && wYPos <= 300){//4
			Send = 4000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 150 && wYPos <= 300){//5
			Send = 5000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 150 && wYPos <= 300){//6
			Send = 6000;
		}	else if (wXPos >= 176 && wXPos <= 330 && wYPos >= 0 && wYPos <= 150){//7
			Send = 7000;
		}	else if (wXPos >= 340 && wXPos <= 480 && wYPos >= 0 && wYPos <= 150){//8
			Send = 8000;
		}	else if (wXPos >= 500 && wXPos <= 650 && wYPos >= 0 && wYPos <= 150){//9
			Send = 9000;
		}	else if (wXPos >= 0 && wXPos <= 160 && wYPos >= 280 && wYPos <= 480){//0
			Send = 0000;
		}	else if (wXPos >= 0 && wXPos <= 160 && wYPos >= 0 && wYPos <= 220){//.
			Send = 7770;
		}	else if (wXPos >= 660 && wXPos <= 800 && wYPos >= 280 && wYPos <= 480){//ENTER
			Send = 1337;
		}	else if (wXPos >= 660 && wXPos <= 800 && wYPos >= 0 && wYPos <= 220){//Backspace
			Send = 4545;
		}
		break;
	default:
		Send = 9999;
		break;
	}
	
	trackpadPressed = Send;
	

	return hr;

}

///////////////////////////////////////////////////////////////////////
// BetterRazerNumpadDlg--Init the Dialog
///////////////////////////////////////////////////////////////////////
BOOL BetterRazerNumpadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	CStringW wstr;
	HRESULT hReturn = S_OK;
	
		::PostMessageA(g_hwndDialog, SW_HIDE, 0, 0L);

	 //Set the icon for this dialog.  The framework does this automatically
	  //when the application's main window is not a dialog
	SetIcon(m_hIcon, FALSE);			// Set big icon
	SetIcon(m_hIcon, TRUE);		// Set small icon

	m_hwndDialog = CDialogEx::CDialog::CWnd::m_hWnd;
	m_RzSBStartStatus = RzSBStart();
	if (m_RzSBStartStatus != S_OK)
	{
		//api won't work.
	}

	InitStructs();
	LoadFileDefaults();
	GetStaticRegions();

	SetDynamicKeys();
	SetTrackPad();
	SetupTimers();

	

	// callbacks set here!
	
    hReturn = RzSBAppEventSetCallback(reinterpret_cast<AppEvent*>(MyAppEventCallback));

	hReturn = RzSBDynamicKeySetCallback(reinterpret_cast<DKEvent*>(MyDynamicKeyCallback));

	hReturn = RzSBGestureSetCallback(reinterpret_cast<GestureEvent*>(MyGestureCallback));

	//hReturn = RzSBKeyboardCaptureSetCallback(reinterpret_cast<KeyboardEvent*>(MyKeyboardCallback));

	InitUIDefaults();

	// Store hwnd to be used in application callback; sends WM_CLOSE to shut down when notified
	g_hwndDialog = m_hWnd;
	
	//
	// Look for the matching RzSBStop() call in FunctionTest.cpp on line 92, after
	// the dlg.DoModal() call completes.
	//

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void BetterRazerNumpadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
//		CAboutDlg dlgAbout;
	//	dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

//////////////////////////////////////////////////////////////////////////////
// Initializing default UI images for DynamicKeys
//////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::InitUIDefaults()
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	HRESULT hReturn = S_OK;
	int i = 0;

	// set last DK to none
	m_Images.LastDKPressed = RZSBSDK_DK_NONE;

	// enable HSCROLL in output box
	//CSOutput.EnableScrollBar(SB_BOTH, ESB_ENABLE_BOTH);
	//CSOutput.SetHorizontalExtent(1200); // allow 1200 pixels hscroll

	for (i=RZSBSDK_DK_1; i<RZSBSDK_DK_INVALID; i++)
	{
		hReturn = RzSBSetImageDynamicKey((_RZSBSDK_DKTYPE)i, RZSBSDK_KEYSTATE_UP, m_Images.m_ImageFilenamesUp[i]);
		
		hReturn = RzSBSetImageDynamicKey((_RZSBSDK_DKTYPE)i, RZSBSDK_KEYSTATE_DOWN, m_Images.m_ImageFilenamesDown[i]);
	}
	//hReturn = RzSBSetImageDynamicKey((_RZSBSDK_DKTYPE)1, RZSBSDK_KEYSTATE_UP, m_Images.m_ImageFilenamesDown[1]);
	//hReturn = RzSBSetImageDynamicKey((_RZSBSDK_DKTYPE)10, RZSBSDK_KEYSTATE_UP, m_Images.m_ImageFilenamesUp[10]);

	hReturn = RzSBSetImageTouchpad(m_Images.m_ImagefilenameNumpad);//this will need to be something else, included in the project prob.
	
	// default is to have all gestures enabled
	RzSBEnableGesture(RZSBSDK_GESTURE_MOVE, true);
	RzSBEnableGesture(RZSBSDK_GESTURE_PRESS, true);
	RzSBEnableGesture(RZSBSDK_GESTURE_TAP, false);
	RzSBEnableGesture(RZSBSDK_GESTURE_FLICK, false);
	RzSBEnableGesture(RZSBSDK_GESTURE_ZOOM, false);
	RzSBEnableGesture(RZSBSDK_GESTURE_ROTATE, false);

	// default is to have all gestures forwarded
	SetOSGestureForwarding(RZSBSDK_GESTURE_MOVE, false);
	SetOSGestureForwarding(RZSBSDK_GESTURE_PRESS, false); //this is able to be changed 
	SetOSGestureForwarding(RZSBSDK_GESTURE_TAP, false);
	SetOSGestureForwarding(RZSBSDK_GESTURE_FLICK, false);
	SetOSGestureForwarding(RZSBSDK_GESTURE_ZOOM, false);
	SetOSGestureForwarding(RZSBSDK_GESTURE_ROTATE, false);
}

//////////////////////////////////////////////////////////////////////////////
//  Initializing files list structure for storing DK Up/Down images
//////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::InitStructs()
{
	CStringW wstr;

	// Init the bitmap states for the DKs to unset, and bitmaps to empty
	int i = 0;

	for (i=RZSBSDK_DK_NONE; i<RZSBSDK_DK_INVALID; i++)
	{
		m_Images.m_DKImagesUp[i].set = false;
		memset(&m_Images.m_ImageFilenamesUp[i], 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

		m_Images.m_DKImagesDown[i].set = false;
		memset(&m_Images.m_ImageFilenamesDown[i], 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	}

	memset(&m_Images.m_ImagefilenameNumpad, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	memset(&m_Images.m_ImagefilenameNumpad2, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	memset(&m_Images.m_ImagefilenameNumpadFree, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

	//
	// set the default for the current down and up images, which
	// is a gray square until they assign something
	//
	memset(m_Images.m_ImagefilenameCurrrentDown, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	memset(m_Images.m_ImagefilenameCurrrentUp, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

	// load the default (unset DK) image
	m_Images.m_DKDefault.bm.LoadBitmap(IDB_DEFAULT_DK_BITMAP);
}

//////////////////////////////////////////////////////////////////////////////
//  Load Default Images
//////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::LoadFileDefaults()
{
	WCHAR tmp[MAX_STRING_LENGTH] = L"";
	WCHAR curpath[MAX_STRING_LENGTH] = L"";
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int i = 0;
	size_t longestfilename = 0, nFNLength = 0;

	DWORD dwPathLength = GetCurrentDirectory(MAX_STRING_LENGTH, curpath);
	if (dwPathLength >= MAX_STRING_LENGTH)
		return;
	
	//set the default for background 
	//wcscpy_s(m_Images.m_ImagefilenameBackground, MAX_STRING_LENGTH, curpath);
    //wcscat_s(m_Images.m_ImagefilenameBackground, MAX_STRING_LENGTH, L"\\imagedata\\background.png");

	// check path + longest filename for length
	for (i=RZSBSDK_DK_1; i<RZSBSDK_DK_INVALID; i++)
	{
		nFNLength = max(wcslen(InitialDKImageFilenames.up_filenames[i]), wcslen(InitialDKImageFilenames.down_filenames[i]));
		if (longestfilename < nFNLength)
			longestfilename = nFNLength;
	}
	
	longestfilename += wcslen(curpath);

	if (MAX_STRING_LENGTH < longestfilename)
	{
		wsprintf(outstring, L"Path + filenames > limit (cannot init image files)!");
		
		return;
	}
	else
	{
		// concatenate the path and files for default images
		for (i=RZSBSDK_DK_1; i<RZSBSDK_DK_INVALID; i++)
		{
			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.up_filenames[i][1]); // skip the "."

			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.up_filenames[i][1]); // skip the "."
			wcscpy_s(InitialDKImageFilenames.up_filenames[i], MAX_STRING_LENGTH, tmp);

			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.down_filenames[i][1]); // skip the "."
			wcscpy_s(InitialDKImageFilenames.down_filenames[i], MAX_STRING_LENGTH, tmp);
		}

		wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
		wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.numpadBG[1]); // skip the "."
		wcscpy_s(InitialDKImageFilenames.numpadBG, MAX_STRING_LENGTH, tmp);
		wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
		wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.numpad2BG[1]); // skip the "."
		wcscpy_s(InitialDKImageFilenames.numpad2BG, MAX_STRING_LENGTH, tmp);
		wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
		wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.numpadFree[1]); // skip the "."
		wcscpy_s(InitialDKImageFilenames.numpadFree, MAX_STRING_LENGTH, tmp);
	}
	/*hFile = CreateFile(InitialDKImageFilenames.numpadBG, GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);*/

	wcscpy_s(m_Images.m_ImagefilenameNumpad,MAX_STRING_LENGTH,InitialDKImageFilenames.numpadBG);
	wcscpy_s(m_Images.m_ImagefilenameNumpad2, MAX_STRING_LENGTH, InitialDKImageFilenames.numpad2BG);
	wcscpy_s(m_Images.m_ImagefilenameNumpadFree, MAX_STRING_LENGTH,	InitialDKImageFilenames.numpadFree);

	for (i=RZSBSDK_DK_1; i<RZSBSDK_DK_INVALID; i++)
	{
		// up
		HANDLE hFile;
		hFile = CreateFile(InitialDKImageFilenames.up_filenames[i], GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			wsprintf(outstring, L"File \"%ws\" does not exist!", InitialDKImageFilenames.up_filenames[i]);
			////Log(outstring);
			CloseHandle(hFile);
			return;
		}
		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwFileSize;
			dwFileSize = GetFileSize(hFile, NULL);

			if(dwFileSize > 0)
			{
				wcscpy_s(m_Images.m_ImageFilenamesUp[i],
							MAX_STRING_LENGTH,
							InitialDKImageFilenames.up_filenames[i]);
			}
			CloseHandle(hFile);
		}
	// down
//				HANDLE hFile;
		hFile = CreateFile(InitialDKImageFilenames.down_filenames[i], GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			wsprintf(outstring, L"File \"%ws\" does not exist!", InitialDKImageFilenames.down_filenames[i]);
			//Log(outstring);
			CloseHandle(hFile);
			return;
		}
		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwFileSize;
			dwFileSize = GetFileSize(hFile, NULL);

			if(dwFileSize > 0)
			{
				wcscpy_s(m_Images.m_ImageFilenamesDown[i],
							MAX_STRING_LENGTH,
							InitialDKImageFilenames.down_filenames[i]);
			}
			CloseHandle(hFile);
		}
	}
	// NUMPAD
	
	return;
}

//////////////////////////////////////////////////////////////////////////////
//  Load Image File based on its full path and name
//////////////////////////////////////////////////////////////////////////////
bool BetterRazerNumpadDlg::LoadFile(LPWSTR pszNewFilename)
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	WCHAR szFileName[MAX_STRING_LENGTH] = L"";
	OPENFILENAME of;

	if (!pszNewFilename)
	{
		pszNewFilename = (LPWSTR)GlobalAlloc(GPTR, 261 * sizeof(WCHAR));
		if (!pszNewFilename)
			return false;
	}

	ZeroMemory(&of, sizeof(of));

	of.lStructSize = sizeof(of);
	of.hwndOwner = CDialogEx::CDialog::CWnd::m_hWnd;//m_hwndDialog;
	of.lpstrFilter = L"PNG Files(*.png)\0*.PNG"
		             L"\0BMP Files(.bmp)\0*.bmp"
		             L"\0JPG Files(.jpg)\0*.jpg"
		             L"\0JPEG Files(.jpeg)\0*.jpeg"
		             L"\0GIF Files(.gif)\0*.gif";
					 //L"\0All Files(*.*)\0*.*\0";
	of.lpstrFile = szFileName;
	of.nMaxFile = MAX_STRING_LENGTH;
	of.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	of.lpstrDefExt = L"PNG";

	BOOL temp = GetOpenFileName(&of);
	if (temp)
	{
		HANDLE hFile;
		hFile = CreateFile(of.lpstrFile, GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			wsprintf(outstring, L"File \"%ws\" does not exist!", of.lpstrFile);
			//Log(outstring);
			CloseHandle(hFile);
			return(false);
		}
		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwFileSize;
			dwFileSize = GetFileSize(hFile, NULL);

			if(dwFileSize > 0)
			{
				wcscpy_s(pszNewFilename, MAX_STRING_LENGTH,  of.lpstrFile);
			}
			CloseHandle(hFile);
		}
	}
	return (0 != temp);
}

///////////////////////////////////////////////////////////////////////////////////////
//  Translate the regions in Windows UI Space to the region in  SwitchBlade LCD
///////////////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::GetStaticRegions()
{
	POINT dlgTopLeft = {0, 0};
	::GetWindowRect(this->m_Images.m_CurrentUp, &m_Images.m_RectUp);
	::GetWindowRect(this->m_Images.m_CurrentDown, &m_Images.m_RectDown);
	::GetWindowRect(this->m_Images.m_CurrentBackground, &m_Images.m_RectBackground);

	// Translate coordinates to more useful coordinates: those that
	// are used on the dialog.
	// In order to do the translation we have to find the top left
	// point (coordinates) of the dialog's client:
	::ClientToScreen(this->m_hWnd, &dlgTopLeft);

	// With these coordinates we can do the translation.
	m_Images.m_RectUp.top    -= dlgTopLeft.y;
	m_Images.m_RectUp.left   -= dlgTopLeft.x;
	m_Images.m_RectUp.bottom = m_Images.m_RectUp.top + SWITCHBLADE_DYNAMIC_KEY_Y_SIZE;
	m_Images.m_RectUp.right  = m_Images.m_RectUp.left + SWITCHBLADE_DYNAMIC_KEY_X_SIZE;

	m_Images.m_RectDown.top    -= dlgTopLeft.y;
	m_Images.m_RectDown.left   -= dlgTopLeft.x;
	m_Images.m_RectDown.bottom = m_Images.m_RectDown.top + SWITCHBLADE_DYNAMIC_KEY_Y_SIZE;
	m_Images.m_RectDown.right  = m_Images.m_RectDown.left + SWITCHBLADE_DYNAMIC_KEY_X_SIZE;

	m_Images.m_RectBackground.top    -= dlgTopLeft.y;
	m_Images.m_RectBackground.left   -= dlgTopLeft.x;
	m_Images.m_RectBackground.bottom = m_Images.m_RectBackground.top + SWITCHBLADE_TOUCHPAD_Y_SIZE;
	m_Images.m_RectBackground.right  = m_Images.m_RectBackground.left + SWITCHBLADE_TOUCHPAD_X_SIZE;
}


//////////////////////////////////////////////////////////////////////////////////
// Checks the filename for one of the accepted file types. we're case insensitive
// since we lowercase everything for comparison
/////////////////////////////////////////////////////////////////////////////////
FILETYPE MyGetFileType(LPWSTR lpszImageFilename)
{
	errno_t tmp = 0;
	FILETYPE ftReturn = ft_invalid;
	size_t nLength = wcslen(lpszImageFilename)+1;
	//
	// secure tolower(). We can do this because the leaf routines all copy the strings to
	// a preallocated buffer.
	//
	tmp = _wcslwr_s(lpszImageFilename, nLength);

	if (wcsstr(lpszImageFilename, L"png"))
		ftReturn = ft_png;
	else if ((wcsstr(lpszImageFilename, L"jpg"))
		 ||  (wcsstr(lpszImageFilename, L"jpeg")))
		ftReturn = ft_jpg;
	else if (wcsstr(lpszImageFilename, L"bmp"))
		ftReturn = ft_bmp;
	else if (wcsstr(lpszImageFilename, L"gif"))
		ftReturn = ft_gif;

	return ftReturn;
}


///////////////////////////////////////////////////////////////////////
// if the updated filename is different than that already in use,
// use the updated filename. Also report whether an update took
// place so that we can know whether to process further based on
// the revised filename
///////////////////////////////////////////////////////////////////////
bool BetterRazerNumpadDlg::UpdateFileStrings(LPWSTR pszNew, LPWSTR pszOld)
{
	bool bResult = false;
	size_t length = 0;

	// check the image string for the DK. If null, just assign
	length = wcslen(pszOld);
	if (0 == length)
	{
		wcscpy_s(pszOld, MAX_STRING_LENGTH, pszNew);
		bResult = true;
	}
	else
	{
		int i = 0;
		// update the key file name if there's a change
		i = wcscmp(pszNew, pszOld);
		if (0 != i)
		{
			wcscpy_s(pszOld, MAX_STRING_LENGTH, pszNew);
			bResult = true;
		}
	}

	return bResult;
}


////////////////////////////////////////////////////////////////////////////////////
// Perform the dynamic key update. we have a file different from the original,
// so send it through the SDK, and report how it worked out for us...
/////////////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::UpdateDynamicKeyImage(RZSBSDK_DKTYPE dk, RZSBSDK_KEYSTATETYPE dkState, LPWSTR filename)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBSetImageDynamicKey(dk, dkState, filename);
	return;
}

/////////////////////////////////////////////////////////////////////////
// Converting  a gesture type to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
RZSBSDK_GESTURETYPEToString(
	RZSBSDK_GESTURETYPE gesture
	)
{
	if (!ValidGesture(gesture))
		return L"Gesture out of range";
	if (!SingleGesture(gesture))
		return L"Compound gesture";

	else if (gesture == RZSBSDK_GESTURE_NONE)
		return L"None";
	else if (gesture == RZSBSDK_GESTURE_MOVE)
		return L"Move";
	else if (gesture == RZSBSDK_GESTURE_PRESS)
		return L"Press";
	else if (gesture == RZSBSDK_GESTURE_TAP)
		return L"Tap";
	else if (gesture == RZSBSDK_GESTURE_FLICK)
		return L"Flick";
	else if (gesture == RZSBSDK_GESTURE_ZOOM)
		return L"Zoom";
	else if (gesture == RZSBSDK_GESTURE_ROTATE)
		return L"Rotate";
	else if (gesture == RZSBSDK_GESTURE_ALL)
		return L"All";

	return L"Completely Bogus Gesture!";
}

/////////////////////////////////////////////////////////////////////////
// Converting the type of dynamic key to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
DKTYPEToString(
	_RZSBSDK_DKTYPE dk
	)
{
	if (!ValidDynamicKey(dk))
		return L"DK value out of range";

	switch (dk) {
	case RZSBSDK_DK_NONE:
			return L"DK (none)";
    case RZSBSDK_DK_1:
		return L"DK 1";
    case RZSBSDK_DK_2:
		return L"DK 2";
    case RZSBSDK_DK_3:
		return L"DK 3";
    case RZSBSDK_DK_4:
		return L"DK 4";
    case RZSBSDK_DK_5:
		return L"DK 5";
    case RZSBSDK_DK_6:
		return L"DK 6";
    case RZSBSDK_DK_7:
		return L"DK 7";
    case RZSBSDK_DK_8:
		return L"DK 8";
    case RZSBSDK_DK_9:
		return L"DK 9";
    case RZSBSDK_DK_10:
		return L"DK 10";
	case RZSBSDK_DK_INVALID:
		return L"invalid dynamic key";
	default:
		return L"undefined dynamic key";
	}
//	return "DKTYPEToString error"; // 4702 compiler warning (unreachable code)
}

/////////////////////////////////////////////////////////////////////////
// Converting the state of dynamic key to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
RZDKSTATEToString(
	RZSBSDK_KEYSTATETYPE dkState
	)
{
	if (!ValidKeyState(dkState))
		return L"DK state out of range";

	switch (dkState) {
    case RZSBSDK_KEYSTATE_NONE:
		return L"Key state (none)";
    case RZSBSDK_KEYSTATE_DOWN:
		return L"Down";
    case RZSBSDK_KEYSTATE_UP:
		return L"Up";
    case RZSBSDK_KEYSTATE_HOLD:
		return L"Hold";
    case RZSBSDK_KEYSTATE_INVALID:
	default:
		return L"invlid dynamic key state";
	}
}

/////////////////////////////////////////////////////////////////////////
// Enable/disable the callback of the keyboard capture for Application
/////////////////////////////////////////////////////////////////////////
//void
//BetterRazerNumpadDlg::SetKeyboardCapture(
//	bool bEnable
//	)
//{
//	HRESULT hReturn = S_OK;
//	WCHAR outstring[MAX_STRING_LENGTH] = L"";
//
//    hReturn = RzSBCaptureKeyboard(bEnable); 
//	//if (RZSB_SUCCESS(hReturn))
//	//{
//	//	wsprintf(outstring,
//	//		L"RzSBCaptureKeyboard(%ws) = RZSB_OK. %ws",
//	//		(bEnable) ? L"True" : L"False", 
// //           (bEnable) ? L"The device keyboard will be captured." : L"The keyboard capture has been released.");
//	//}
//	//else
//	//{
//	//	wsprintf(outstring,
//	//		L"RzSBCaptureKeyboard(%ws) = 0x%08lx, additional status = 0x%08lx",
//	//		(bEnable) ? L"True" : L"False",
//	//		hReturn,
//	//		GetLastError());
//	//}
//}

/////////////////////////////////////////////////////////////////////////
// Enable/disable the callback of the gesture control for Application
/////////////////////////////////////////////////////////////////////////
void
BetterRazerNumpadDlg::SetGestureNotify(
	RZSBSDK_GESTURETYPE Gesture,
	bool bEnable
	)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBEnableGesture(Gesture, bEnable);
}

/////////////////////////////////////////////////////////////////////////
// Enable/disable the callback of the gesture control for OS
/////////////////////////////////////////////////////////////////////////
void
BetterRazerNumpadDlg::SetOSGestureForwarding(
	RZSBSDK_GESTURETYPE Gesture,
	bool bEnable
	)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBEnableOSGesture(Gesture, bEnable);
	
}
void BetterRazerNumpadDlg::SetTrackPad()
{
	for (int i = 0; i<= 12;i++)
	{
		TP[i].fFirstPass = true;
		TP[i].fPressPass = true;
		SetTrackPadKeys(i);
	}
}

void BetterRazerNumpadDlg::SetAllTrackPadKeys()
{
	for (int i = 0; i<= 12;i++)
	{
		SetTrackPadKeys(i);
	}
}

void BetterRazerNumpadDlg::SetTrackPadKeys(int i)
{
	//a few here equal the same, and that's incause i later want to change their functionality.
	switch (i)
	{
		case 0:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD1 : VK_END;
			break;
		case 1:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD2 : VK_DOWN;
			break;
		case 2:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD3 : VK_NEXT;
			break;
		case 3:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD4 : VK_LEFT;
			break;
		case 4:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD5 : VK_NUMPAD5;
			break;
		case 5:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD6 : VK_RIGHT;
			break;
		case 6:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD7 : VK_HOME;
			break;
		case 7:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD8 : VK_UP;
			break;
		case 8:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD9 : VK_PRIOR;
			break;
		case 9:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_NUMPAD0 : VK_INSERT;
			break;
		case 10:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_RETURN : VK_RETURN;
			break;
		case 11:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_DECIMAL : VK_DELETE;
			break;
		case 12:
			TP[i].VirtualKey = (ISNUMLOCKON) ? VK_BACK : VK_BACK;
			break;
	}

}

void BetterRazerNumpadDlg::SetDynamicKeys()
{
	for (int i = 0; i<= 9;i++)
	{

		switch (i){
		case 0:
				DK[i].Key =  RZSBSDK_DK_1; //numlock on/off
				DK[i].VirtualKey = VK_NUMLOCK;
				break;
		case 1:     
				DK[i].Key =  RZSBSDK_DK_2;///	
				DK[i].VirtualKey = VK_DIVIDE;
				break;
		case 2:     
				DK[i].Key =  RZSBSDK_DK_3;//*	
				DK[i].VirtualKey = VK_MULTIPLY;
				break;
		case 3:     
				DK[i].Key =  RZSBSDK_DK_4;//-	
				DK[i].VirtualKey = VK_MULTIPLY;
				break;
		case 4:     
				DK[i].Key =  RZSBSDK_DK_5;//+	
				DK[i].VirtualKey = VK_ADD;
				break;
		/*case 5:     
				DK[i].Key =  RZSBSDK_DK_6;//	
				DK[i].VirtualKey = ; 
				
		case 6:     
				DK[i].Key =  RZSBSDK_DK_7;//	
				DK[i].VirtualKey = ; 
				break;
				
		case 7:     
				DK[i].Key =  RZSBSDK_DK_8;//	
				DK[i].VirtualKey = ; 
				break;*/
				
		case 8:     
				DK[i].Key =  RZSBSDK_DK_9;//	
				//DK[i].VirtualKey = 9; // OpenCalc(); 
				break;
				
		case 9:     
				DK[i].Key =  RZSBSDK_DK_10; //free press ON/OFF	
				//DK[i].VirtualKey = 10; //SetFreePress()
				break;
		}
		DK[i].fFirstPass = true;
		DK[i].fPressPass = true;
		DK[i].CurrentState = RZSBSDK_KEYSTATE_UP; //not pressed
		DK[i].PreviousState = RZSBSDK_KEYSTATE_UP;
		SetTimer(i+1,1,NULL);
	}
}

void SetFreePress()
{
	RzSBEnableGesture(RZSBSDK_GESTURE_TAP, FreePress);
	RzSBEnableGesture(RZSBSDK_GESTURE_MOVE, !FreePress);//will always be enabled
	RzSBEnableGesture(RZSBSDK_GESTURE_PRESS, !FreePress);//will always be enabled
	RzSBEnableGesture(RZSBSDK_GESTURE_FLICK, FreePress);
	RzSBEnableGesture(RZSBSDK_GESTURE_ZOOM, FreePress);
	RzSBEnableGesture(RZSBSDK_GESTURE_ROTATE, FreePress);

	//// enable all the gesture callbacks	
	RzSBEnableOSGesture(RZSBSDK_GESTURE_MOVE, FreePress); 
	RzSBEnableOSGesture(RZSBSDK_GESTURE_PRESS, FreePress); 
	RzSBEnableOSGesture(RZSBSDK_GESTURE_TAP, FreePress);
	RzSBEnableOSGesture(RZSBSDK_GESTURE_FLICK, FreePress);
	RzSBEnableOSGesture(RZSBSDK_GESTURE_ZOOM, FreePress);
	RzSBEnableOSGesture(RZSBSDK_GESTURE_ROTATE, FreePress);

}

//////////////////////////////////////////////////////////////////////////////
//  Render DK Image for DK UI
//////////////////////////////////////////////////////////////////////////////
void BetterRazerNumpadDlg::DrawDKImage(RZSBSDK_KEYSTATETYPE dkState, LPCWSTR pszwFilename)
{
	HRESULT hr = S_OK; 
	CImage imageFile; 
    HDC hDC;
	HDC hSurfaceDC = ::GetDC(m_hwndDialog);
    HDC hScreenDC = CreateCompatibleDC(hSurfaceDC);
	RECT rc;
	if (dkState == RZSBSDK_KEYSTATE_UP)
		memcpy(&rc, &m_Images.m_RectUp, sizeof(RECT));
	else
		memcpy(&rc, &m_Images.m_RectDown, sizeof(RECT));

	hr = imageFile.Load(pszwFilename);

	CRect rect(CPoint(0, 0), CSize(SWITCHBLADE_DYNAMIC_KEY_X_SIZE, SWITCHBLADE_DYNAMIC_KEY_Y_SIZE));

    if (hr != S_OK) return; 

    hDC = imageFile.GetDC(); 

    HDC hMemDC = CreateCompatibleDC(hDC);
    if(hMemDC)
    {
        void* pBits = NULL;

        BITMAPV5HEADER bmi = {0};
        bmi.bV5Size        = sizeof(BITMAPV5HEADER);

        bmi.bV5Width       = rect.Width();
        bmi.bV5Height      = rect.Height();

        bmi.bV5SizeImage   = bmi.bV5Width * bmi.bV5Height * 2;
        bmi.bV5CSType      = LCS_WINDOWS_COLOR_SPACE;
        bmi.bV5Intent      = LCS_GM_IMAGES;
        bmi.bV5BitCount    = 16;
        bmi.bV5Planes      = 1;
        bmi.bV5Compression = BI_BITFIELDS;
        bmi.bV5RedMask     = 0x0000F800;   // 5
        bmi.bV5GreenMask   = 0x000007E0;   // 6
        bmi.bV5BlueMask    = 0x0000001F;   // 5

        HBITMAP hBitMap = CreateDIBSection(hDC, (BITMAPINFO*)&bmi, DIB_RGB_COLORS, &pBits, 0, 0);
        if(hBitMap)
        {
            HBITMAP hOldBitMap = (HBITMAP)SelectObject(hMemDC, hBitMap);
			BitBlt(hSurfaceDC, rc.left, rc.top, bmi.bV5Width, bmi.bV5Height, hDC, 0, 0, SRCCOPY);
			SelectObject(hMemDC, hOldBitMap);
            DeleteObject(hBitMap);
        }
        DeleteDC(hMemDC);
        DeleteDC(hScreenDC);
        DeleteDC(hSurfaceDC);
    }

    imageFile.ReleaseDC(); 
}

void BetterRazerNumpadDlg::SetupTimers()
{
	SetTimer(1000,1,NULL);
	SetTimer(2000,1,NULL);
	SetTimer(3000,1,NULL);
	SetTimer(4000,1,NULL);
	SetTimer(5000,1,NULL);
	SetTimer(6000,1,NULL);
	SetTimer(7000,1,NULL);
	SetTimer(8000,1,NULL);
	SetTimer(9000,1,NULL);
	SetTimer(0000,1,NULL);
	SetTimer(1337,1,NULL);
	SetTimer(7770,1,NULL);
	SetTimer(4545,1,NULL);
}

void BetterRazerNumpadDlg::OpenCalc()
{
	DK[8].fFirstPass = false;
	ZeroMemory(&StartupInfo, sizeof(StartupInfo));
	StartupInfo.cb = sizeof StartupInfo ; //Only compulsory field

	if (fCalcOpen)
	{
						
		TerminateProcess(ProcessInfo.hProcess,PROCESS_TERMINATE);	
		CloseHandle(ProcessInfo.hThread);
		CloseHandle(ProcessInfo.hProcess);
		fCalcOpen = false;
	}
	else
	{
		if(CreateProcess(L"c:\\windows\\System32\\calc.exe", NULL, 
			NULL,NULL,FALSE,0,NULL,
			NULL,&StartupInfo,&ProcessInfo))
		{ 
			fCalcOpen = true;
		}  
	}
}