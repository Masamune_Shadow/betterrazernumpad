
// TestDlg.h : header file
//

#pragma once
#pragma warning(push, 1)
#include "afxwin.h"
#pragma warning(pop)
#include <math.h>
#include "SwitchBlade.h"		// SwitchBlade SDK support

/******************************************************************
*                                                                 *
*  Macros                                                         *
*                                                                 *
******************************************************************/

template<class Interface>
inline void
SafeRelease(
    Interface **ppInterfaceToRelease
    )
{
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}
void SetFreePress();
#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif

#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif

typedef HRESULT (STDMETHODCALLTYPE AppEvent)(RZSBSDK_EVENTTYPETYPE, DWORD, DWORD);
typedef HRESULT (STDMETHODCALLTYPE DKEvent)(RZSBSDK_DKTYPE, RZSBSDK_KEYSTATETYPE);
typedef HRESULT (STDMETHODCALLTYPE GestureEvent)(RZSBSDK_GESTURETYPE,DWORD,WORD,WORD,WORD);
//typedef HRESULT (STDMETHODCALLTYPE KeyboardEvent)(UINT uMsg, WPARAM wParam, LPARAM lParam);

typedef struct _BM_
{
	bool set;
	CBitmap bm;
} BM, *PBM;


class CImages : public CDialogEx
{
public:
	// coords on dialog of these elements
	RECT m_RectUp, m_RectDown, m_RectBackground;

	// default image to use for a DK when there is none available
	BM m_DKDefault;

	RZSBSDK_DKTYPE LastDKPressed;

	// filename associated with the images currently displayed on the statics
	WCHAR m_ImagefilenameCurrrentUp[MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameCurrrentDown[MAX_STRING_LENGTH];

	// filenames associated with the set images for the DKs
	WCHAR m_ImageFilenamesUp[RZSBSDK_DK_INVALID][MAX_STRING_LENGTH];
	WCHAR m_ImageFilenamesDown[RZSBSDK_DK_INVALID][MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameNumpad[MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameNumpad2[MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameNumpadFree[MAX_STRING_LENGTH];

	// images currently displayed
	CStatic m_CurrentUp;
	CStatic m_CurrentDown;
	CStatic m_CurrentBackground;

	// the undisplayed (but cached) images we have for the dynamic keys
	BM m_DKImagesUp[RZSBSDK_DK_INVALID];
	BM m_DKImagesDown[RZSBSDK_DK_INVALID];
};

// BetterRazerNumpadDlg dialog
class BetterRazerNumpadDlg : public CDialogEx
{
// Construction
public:
	BetterRazerNumpadDlg(CWnd* pParent = NULL);	// standard constructor
	HWND       m_hwndDialog;
	HWND       m_hOutput;
	HINSTANCE  m_ghInstance;
	HRESULT    m_RzSBStartStatus;	// status from RzSBStart(), called in FunctionTest.cpp
// Dialog Data
	enum { IDD = IDD_TEST_DIALOG };
// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	//afx_msg void OnPaint();
	
	
	DECLARE_MESSAGE_MAP()

	void InitUIDefaults(void);
	void InitStructs(void);
	void LoadFileDefaults(void);
	bool UpdateFileStrings(LPWSTR, LPWSTR);
	void UpdateDynamicKeyImage(RZSBSDK_DKTYPE, RZSBSDK_KEYSTATETYPE, LPWSTR);
	void SetGestureNotify(RZSBSDK_GESTURETYPE, bool);
	void SetOSGestureForwarding(RZSBSDK_GESTURETYPE, bool);
    //void SetKeyboardCapture(bool);
	void GetStaticRegions(void);
public:
	static HRESULT ApplicationEventCallback(RZSBSDK_EVENTTYPETYPE, DWORD, DWORD);
	static HRESULT DynamicKeyEventCallback(RZSBSDK_DKTYPE, RZSBSDK_KEYSTATETYPE);
	static HRESULT GestureEventCallback(RZSBSDK_GESTURETYPE,DWORD,WORD,WORD,WORD);
    //static HRESULT KeyboardEventCallback(UINT uMsg, WPARAM wParam, LPARAM lParam);
	static void DynamicKeyTest(int Send);
	SHORT m_ImageSizes[2];
	LPBYTE m_ImageData[2];
	virtual void SetDynamicKeys();
	virtual void SetTrackPad();
	virtual void SetTrackPadKeys(int i);
	virtual void SetAllTrackPadKeys();
	virtual void SetupTimers();
	virtual void OpenCalc();
private:
	bool LoadFile(LPWSTR);
	void DrawDKImage(RZSBSDK_KEYSTATETYPE, LPCWSTR);
    //void DrawBackgroundImage(LPCWSTR);
	void OnTimer(UINT_PTR nIDEvent);
	CImages m_Images;
public:
	
};