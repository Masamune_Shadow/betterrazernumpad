//////////////////////////////////////////////////////////////////////////////////////
// TestDlg.cpp : implementation file
//////////////////////////////////////////////////////////////////////////////////////
#pragma warning(push, 0)
#include "stdafx.h"
#include "afxdialogex.h"
#pragma warning(pop)
#include "BetterRazerNumpad.h"
#include "NumpadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

extern LPCWSTR DKTYPEToString(RZDKTYPE);                     // debugging support
extern LPCWSTR RZDKSTATEToString(RZDKSTATETYPE);             // debugging support
extern LPCWSTR RZSDKGESTURETYPEToString(RZSDKGESTURETYPE);   // debugging support

HWND g_hwndDialog = NULL; // used to allow application termination event to send WM_CLOSE to hwnd

////////////////////////////////////////////////////////////////////////////////////////////////////
// Custom type used in this module only
////////////////////////////////////////////////////////////////////////////////////////////////////
typedef enum _FILE_TYPE_ {
	ft_invalid = 0,
	ft_bmp,
	ft_gif,
	ft_jpg,
	ft_png,
	ft_undefined
} FILETYPE;

//////////////////////////////////////////////////////////////////////////////////////////////////
// A image file list for  rendering Up/Down of Dynamic Key
//////////////////////////////////////////////////////////////////////////////////////////////////
typedef struct _file_list_
{
	WCHAR up_filenames[RZDYNAMICKEY_UNDEFINED][MAX_STRING_LENGTH];
	WCHAR down_filenames[RZDYNAMICKEY_UNDEFINED][MAX_STRING_LENGTH];
} file_list;

file_list InitialDKImageFilenames = {
	{
		L"",
		L".\\imagedata\\06.png",
		L".\\imagedata\\07.png",
		L".\\imagedata\\08.png",
		L".\\imagedata\\09.png",
		L".\\imagedata\\10.png",
		L".\\imagedata\\01.png",
		L".\\imagedata\\02.png",
		L".\\imagedata\\03.png",
		L".\\imagedata\\04.png",
		L".\\imagedata\\05.png"
	},
	{
		L"",
		L".\\imagedata\\06_down.png",
		L".\\imagedata\\07_down.png",
		L".\\imagedata\\08_down.png",
		L".\\imagedata\\09_down.png",
		L".\\imagedata\\10_down.png",
		L".\\imagedata\\01_down.png",
		L".\\imagedata\\02_down.png",
		L".\\imagedata\\03_down.png",
		L".\\imagedata\\04_down.png",
		L".\\imagedata\\05_down.png"
	}
};


void Log(LPCWSTR);
CListBox CSOutput;

////////////////////////////////////////////////////////////////////
// Adds a string to the output box, and scrolls to it if neccessary
///////////////////////////////////////////////////////////////////
void Log(LPCWSTR pszwString)
{
	CSOutput.AddString(pszwString);

	int nLast = CSOutput.GetCount();
	nLast = (nLast) ? nLast - 1 : 0;

	CSOutput.SetCurSel(nLast);
}

//////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
//////////////////////////////////////////////////////////////////////
class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();
	~CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About
/////////////////////////////////////////////////////////////////////
CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

/////////////////////////////////////////////////////////////////////
// CAboutDlg destructor
/////////////////////////////////////////////////////////////////////
CAboutDlg::~CAboutDlg()
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////
// NumpadDlg dialog
//////////////////////////////////////////////////////////////
NumpadDlg::NumpadDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(NumpadDlg::IDD, pParent),
    m_hwndDialog(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_hwndDialog = CDialogEx::CDialog::CWnd::m_hWnd;
	m_hWnd = (HWND)*pParent;
}


void NumpadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_SDK_OUTPUT, CSOutput);
	DDX_Control(pDX, IDC_DK_UP_IMAGE, m_Images.m_CurrentUp);
	DDX_Control(pDX, IDC_DK_DOWN_IMAGE, m_Images.m_CurrentDown);
	DDX_Control(pDX, IDC_BACKGROUND_IMAGE, m_Images.m_CurrentBackground);
	DDX_Control(pDX, IDC_COMBO_PRESS, m_PressCombo);
	DDX_Control(pDX, IDC_COMBO_TAP, m_TapCombo);
	DDX_Control(pDX, IDC_COMBO_FLICK, m_FlickCombo);
	DDX_Control(pDX, IDC_COMBO_ZOOM, m_ZoomCombo);
	DDX_Control(pDX, IDC_COMBO_ROTATE, m_RotateCombo);
}

BEGIN_MESSAGE_MAP(NumpadDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_COMMAND_RANGE(IDC_BUTTON_DK1, IDC_BUTTON_DK10, &NumpadDlg::OnBnClickedButton)
	ON_COMMAND_RANGE(IDC_BUTTON_LOAD_UP, IDC_BUTTON_LOAD_DOWN, &NumpadDlg::OnBnClickedLoadImage)
	ON_BN_CLICKED(IDC_BUTTON11, &NumpadDlg::OnClickedUpdateButton)
	ON_CBN_SELCHANGE(IDC_COMBO_PRESS, &NumpadDlg::OnCbnSelchangeComboPress)
	ON_CBN_SELCHANGE(IDC_COMBO_TAP, &NumpadDlg::OnCbnSelchangeComboTap)
	ON_CBN_SELCHANGE(IDC_COMBO_FLICK, &NumpadDlg::OnCbnSelchangeComboFlick)
	ON_CBN_SELCHANGE(IDC_COMBO_ZOOM, &NumpadDlg::OnCbnSelchangeComboZoom)
	ON_CBN_SELCHANGE(IDC_COMBO_ROTATE, &NumpadDlg::OnCbnSelchangeComboRotate)
END_MESSAGE_MAP()

//////////////////////////////////////////////////////////////////////////////
// App Event Callback  -- fires when the AppManager wants us to quit
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE MyAppEventCallback(
    RZSDKAPPEVENTTYPE rzEvent,
	DWORD dwAppMode,
	DWORD dwProcessID
	)
{
	HRESULT hr = S_OK;

    hr = NumpadDlg::ApplicationEventCallback(rzEvent, dwAppMode, dwProcessID);

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display gesture message in the current dialog box
// when MyGestureCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT
NumpadDlg::ApplicationEventCallback(
    RZSDKAPPEVENTTYPE rzEvent,
	DWORD dwAppMode,
	DWORD dwProcessID
	)
{
	// Note: Should not call Log, which updates dialog controls; we could get here as a result of being shut down, and they may be gone
	HRESULT hr = S_OK;
	
	// If you manage state based on this event, handle it here.
    if (RZSDKAPPEVENTTYPE_APPMODE != rzEvent)
    {
    }

	// If you manage state based on this event, handle it here.
	if ((HWND)NULL != g_hwndDialog)
		::PostMessageA(g_hwndDialog, WM_CLOSE, 0, 0L);

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Local dynamickey callback  used in SBSDK--RzSBDynamicKeySetCallback
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE
MyDynamicKeyCallback(RZDKTYPE dk, RZDKSTATETYPE dkState)
{
	HRESULT hr = S_OK;

	hr = NumpadDlg::DynamicKeyEventCallback(dk, dkState);

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display DynamicKey message in the current dialog box
// when MyDynamicKeyCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT
NumpadDlg::DynamicKeyEventCallback(RZDKTYPE dk, RZDKSTATETYPE dkState)
{
	HRESULT hr = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	wsprintf(outstring,
		L"Dynamic Key (%ws, %ws) received.",
		DKTYPEToString(dk),
		RZDKSTATEToString(dkState)
		);

	Log(outstring);

	// If you manage state based on this event, handle it here.

	return hr;
}

//////////////////////////////////////////////////////////////////////////////
// Local gesture callback  used in SBSDK--RzSBGestureSetCallback
//////////////////////////////////////////////////////////////////////////////
HRESULT STDMETHODCALLTYPE MyGestureCallback(
    RZSDKGESTURETYPE gesture,
	DWORD dwParameters,
	WORD wXPos,
	WORD wYPos,
	WORD wZPos
	)
{
	HRESULT hr = S_OK;

	hr = NumpadDlg::GestureEventCallback(gesture,dwParameters,wXPos,wYPos,wZPos);

	return hr;
}

/////////////////////////////////////////////////////////////////////////////
// Display gesture message in the current dialog box
// when MyGestureCallback is called
//////////////////////////////////////////////////////////////////////////////
HRESULT
NumpadDlg::GestureEventCallback(
    RZSDKGESTURETYPE gesture,
	DWORD dwParameters,
	WORD wXPos,
	WORD wYPos,
	WORD wZPos
	)
{
	HRESULT hr = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	WCHAR szFlickDirections[5][20] = { L"unspecified", L"left", L"right", L"up", L"down" };

	switch (gesture) {
	case RZGESTURE_INVALID:
		wsprintf(outstring, L"RZGESTURE_INVALID received!");
		break;
	case RZGESTURE_NONE:
		wsprintf(outstring, L"RZGESTURE_NONE received!");
		break;
	case RZGESTURE_PRESS:
		wsprintf(outstring, L"Press(%ws,%d,%d)", (dwParameters == 0) ? L"Up" : L"Down", wXPos, wYPos);
		break;
	case RZGESTURE_TAP:
		wsprintf(outstring, L"Tap(%d,%d,%d)", dwParameters, wXPos, wYPos);
		break;
	case RZGESTURE_FLICK:
		wsprintf(outstring, L"Flick(%ws)", szFlickDirections[wZPos]);
		break;
	case RZGESTURE_ZOOM:
		wsprintf(outstring, L"Zoom(%ws)", (dwParameters == 1) ? L"In" : L"Out");
		break;
	case RZGESTURE_ROTATE:
		wsprintf(outstring, L"Rotate(%wsclockwise)", (dwParameters != 2) ? L"" : L"anti");
		break;
	case RZGESTURE_ALL:
		wsprintf(outstring, L"RZGESTURE_ALL !!!");
		break;
	case RZGESTURE_UNDEFINED:
	default:
		wsprintf(outstring, L"RZGESTURE_UNDEFINED (bogus gesture)");
		break;
	}

	Log(outstring);

	// If you manage state based on this event, handle it here.

	return hr;

}

///////////////////////////////////////////////////////////////////////
// NumpadDlg--Init the Dialog
///////////////////////////////////////////////////////////////////////
BOOL NumpadDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	CStringW wstr;
	HRESULT hReturn = S_OK;
	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, FALSE);			// Set big icon
	SetIcon(m_hIcon, TRUE);		// Set small icon

	m_hwndDialog = CDialogEx::CDialog::CWnd::m_hWnd;
	if (m_RzSBStartStatus != S_OK)
	{
		wsprintf(outstring, L"RzSBStart() failed, status=0x%08lx!", m_RzSBStartStatus);
		Log(outstring);
		wsprintf(outstring, L"You need to find out why RzSBStart() is failing!");
		Log(outstring);
		wsprintf(outstring, L"--------From this point on, none of the SwitchBlade SDK API calls will work!--------");
		Log(outstring);
	}

	InitStructs();
	LoadFileDefaults();
	GetStaticRegions();

	// callbacks set here!

    hReturn = RzSBAppEventSetCallback(reinterpret_cast<AppEvent*>(MyAppEventCallback));

	hReturn = RzSBDynamicKeySetCallback(reinterpret_cast<DKEvent*>(MyDynamicKeyCallback));

	hReturn = RzSBGestureSetCallback(reinterpret_cast<GestureEvent*>(MyGestureCallback));

	InitUIDefaults();

	// Store hwnd to be used in application callback; sends WM_CLOSE to shut down when notified
	g_hwndDialog = m_hWnd;

	//
	// Look for the matching RzSBStop() call in BetterRazerNumpad.cpp on line 92, after
	// the dlg.DoModal() call completes.
	//

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void NumpadDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

/////////////////////////////////////////////////////////////////////////////
// Paint or  refresh the controls in the dialog box.
// For MFC applications using the document/view model,
// this is automatically done for you by the framework.
//////////////////////////////////////////////////////////////////////////////

void NumpadDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//////////////////////////////////////////////////////////////////////////////////////
// The system calls this function to obtain the cursor to display while the user drags
// the minimized window.
//////////////////////////////////////////////////////////////////////////////////////
HCURSOR NumpadDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


////////////////////////////////////////////////////////////////////
// Click one of the DK buttons to do one of two things:
// inspect current images or update images with either dropped or 
// browsed-for files...
/////////////////////////////////////////////////////////////////////
void NumpadDlg::OnBnClickedButton(UINT nID)
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	UINT index = nID - IDC_BUTTON_DK1;

	m_Images.LastDKPressed = RZDKTYPE(index + RZDYNAMICKEY_DK1);

	// clear the current images, because we expect the DK's images to go in the statics...
	wcscpy_s(m_Images.m_ImagefilenameCurrrentUp, L"");
	wcscpy_s(m_Images.m_ImagefilenameCurrrentDown, L"");

	wsprintf(outstring, L"%ws selected", DKTYPEToString(m_Images.LastDKPressed));
	Log(outstring);

	wsprintf(outstring, L"Up image is %ws", m_Images.m_ImageFilenamesUp[m_Images.LastDKPressed]);
	Log(outstring);

	wsprintf(outstring, L"Down image is %ws", m_Images.m_ImageFilenamesDown[m_Images.LastDKPressed]);
	Log(outstring);

	DrawDKImage(RZSDKSTATE_UP, m_Images.m_ImageFilenamesUp[m_Images.LastDKPressed]);
	DrawDKImage(RZSDKSTATE_DOWN, m_Images.m_ImageFilenamesDown[m_Images.LastDKPressed]);
}

//////////////////////////////////////////////////////////////////////////////
// Initializing default UI images for DynamicKeys
//////////////////////////////////////////////////////////////////////////////
void NumpadDlg::InitUIDefaults()
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	HRESULT hReturn = S_OK;
	int i = 0;

	// set last DK to invalid
	m_Images.LastDKPressed = RZDYNAMICKEY_INVALID;

	// enable HSCROLL in output box
	CSOutput.EnableScrollBar(SB_BOTH, ESB_ENABLE_BOTH);
	CSOutput.SetHorizontalExtent(1200); // allow 1200 pixels hscroll

	for (i=RZDYNAMICKEY_DK1; i<RZDYNAMICKEY_UNDEFINED; i++)
	{
		hReturn = RzSBSetImageDynamicKey((RZDKTYPE)i, RZSDKSTATE_UP, m_Images.m_ImageFilenamesUp[i]);
		if (RZSB_FAILED(hReturn))
		{
			wsprintf(outstring, L"Setting %ws with UP image from %ws failed, status=0x%08lx, additional status=0x%08lx",
				DKTYPEToString((RZDKTYPE)i),
				m_Images.m_ImageFilenamesUp[i],
				hReturn,
				GetLastError()
				);
			Log(outstring);
		}
		hReturn = RzSBSetImageDynamicKey((RZDKTYPE)i, RZSDKSTATE_DOWN, m_Images.m_ImageFilenamesDown[i]);
		if (RZSB_FAILED(hReturn))
		{
			wsprintf(outstring, L"Setting %ws with DOWN image from %ws failed, status=0x%08lx, additional status=0x%08lx",
				DKTYPEToString((RZDKTYPE)i),
				m_Images.m_ImageFilenamesDown[i],
				hReturn,
				GetLastError()
				);
			Log(outstring);
		}
	}

	// default is to have all gesture notifications enabled
	// We don't make the direct API calls because we want to init the controls
	// so they reflect the actual state of the gesture notifications...
	m_PressCombo.SetCurSel(0);  // set the choice to enable press notifications
	m_TapCombo.SetCurSel(0);    // set the choice to enable tap notifications
	m_FlickCombo.SetCurSel(0);  // set the choice to enable flick notifications
	m_ZoomCombo.SetCurSel(0);   // set the choice to enable zoom notifications
	m_RotateCombo.SetCurSel(0); // set the choice to enable rotate notifications

	// default is to have all gestures enabled
	RzSBGestureEnable(RZGESTURE_PRESS, true);
	RzSBGestureEnable(RZGESTURE_TAP, true);
	RzSBGestureEnable(RZGESTURE_FLICK, true);
	RzSBGestureEnable(RZGESTURE_ZOOM, true);
	RzSBGestureEnable(RZGESTURE_ROTATE, true);

	// enable all the gesture callbacks
	RzSBGestureSetNotification(RZGESTURE_PRESS, true);
	RzSBGestureSetNotification(RZGESTURE_TAP, true);
	RzSBGestureSetNotification(RZGESTURE_FLICK, true);
	RzSBGestureSetNotification(RZGESTURE_ZOOM, true);
	RzSBGestureSetNotification(RZGESTURE_ROTATE, true);

	// default is to have all gestures forwarded
	SetOSGestureForwarding(RZGESTURE_PRESS, true);
	SetOSGestureForwarding(RZGESTURE_TAP, true);
	SetOSGestureForwarding(RZGESTURE_FLICK, true);
	SetOSGestureForwarding(RZGESTURE_ZOOM, true);
	SetOSGestureForwarding(RZGESTURE_ROTATE, true);
}

//////////////////////////////////////////////////////////////////////////////
//  Initializing files list structure for storing DK Up/Down images
//////////////////////////////////////////////////////////////////////////////
void NumpadDlg::InitStructs()
{
	CStringW wstr;

	// Init the bitmap states for the DKs to unset, and bitmaps to empty
	int i = 0;
	for (i=RZDYNAMICKEY_INVALID; i<RZDYNAMICKEY_UNDEFINED; i++)
	{
		m_Images.m_DKImagesUp[i].set = false;
		memset(&m_Images.m_ImageFilenamesUp[i], 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

		m_Images.m_DKImagesDown[i].set = false;
		memset(&m_Images.m_ImageFilenamesDown[i], 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	}
	memset(&m_Images.m_ImagefilenameBackground, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

	//
	// set the default for the current down and up images, which
	// is a gray square until they assign something
	//
	memset(m_Images.m_ImagefilenameCurrrentDown, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);
	memset(m_Images.m_ImagefilenameCurrrentUp, 0, sizeof(WCHAR) * MAX_STRING_LENGTH);

	// load the default (unset DK) image
	m_Images.m_DKDefault.bm.LoadBitmap(IDB_DEFAULT_DK_BITMAP);
}

//////////////////////////////////////////////////////////////////////////////
//  Load Default Images
//////////////////////////////////////////////////////////////////////////////
void NumpadDlg::LoadFileDefaults()
{
	WCHAR tmp[MAX_STRING_LENGTH] = L"";
	WCHAR curpath[MAX_STRING_LENGTH] = L"";
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int i = 0;
	size_t longestfilename = 0, nFNLength = 0;

	DWORD dwPathLength = GetCurrentDirectory(MAX_STRING_LENGTH, curpath);
	if (dwPathLength >= MAX_STRING_LENGTH)
		return;

	// check path + longest filename for length
	for (i=RZDYNAMICKEY_DK1; i<RZDYNAMICKEY_UNDEFINED; i++)
	{
		nFNLength = max(wcslen(InitialDKImageFilenames.up_filenames[i]), wcslen(InitialDKImageFilenames.down_filenames[i]));
		if (longestfilename < nFNLength)
			longestfilename = nFNLength;
	}
	longestfilename += wcslen(curpath);

	if (MAX_STRING_LENGTH < longestfilename)
	{
		wsprintf(outstring, L"Path + filenames > limit (cannot init image files)!");
		Log(outstring);
		return;
	}
	else
	{
		// concatenate the path and files for default images
		for (i=RZDYNAMICKEY_DK1; i<RZDYNAMICKEY_UNDEFINED; i++)
		{
			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.up_filenames[i][1]); // skip the "."

			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.up_filenames[i][1]); // skip the "."
			wcscpy_s(InitialDKImageFilenames.up_filenames[i], MAX_STRING_LENGTH, tmp);

			wcscpy_s(tmp, MAX_STRING_LENGTH, curpath);
			wcscat_s(tmp, MAX_STRING_LENGTH, &InitialDKImageFilenames.down_filenames[i][1]); // skip the "."
			wcscpy_s(InitialDKImageFilenames.down_filenames[i], MAX_STRING_LENGTH, tmp);
		}
	}

	for (i=RZDYNAMICKEY_DK1; i<RZDYNAMICKEY_UNDEFINED; i++)
	{
		// up
		{
			HANDLE hFile;
			hFile = CreateFile(InitialDKImageFilenames.up_filenames[i], GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if (hFile == INVALID_HANDLE_VALUE)
			{
				wsprintf(outstring, L"File \"%ws\" does not exist!", InitialDKImageFilenames.up_filenames[i]);
				Log(outstring);
				CloseHandle(hFile);
				return;
			}
			if (hFile != INVALID_HANDLE_VALUE)
			{
				DWORD dwFileSize;
				dwFileSize = GetFileSize(hFile, NULL);

				if(dwFileSize > 0)
				{
					wcscpy_s(m_Images.m_ImageFilenamesUp[i],
						     MAX_STRING_LENGTH,
							 InitialDKImageFilenames.up_filenames[i]);
				}
				CloseHandle(hFile);
			}
		}
		// down
		{
			HANDLE hFile;
			hFile = CreateFile(InitialDKImageFilenames.down_filenames[i], GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

			if (hFile == INVALID_HANDLE_VALUE)
			{
				wsprintf(outstring, L"File \"%ws\" does not exist!", InitialDKImageFilenames.down_filenames[i]);
				Log(outstring);
				CloseHandle(hFile);
				return;
			}
			if (hFile != INVALID_HANDLE_VALUE)
			{
				DWORD dwFileSize;
				dwFileSize = GetFileSize(hFile, NULL);

				if(dwFileSize > 0)
				{
					wcscpy_s(m_Images.m_ImageFilenamesDown[i],
						     MAX_STRING_LENGTH,
							 InitialDKImageFilenames.down_filenames[i]);
				}
				CloseHandle(hFile);
			}
		}
	}
	return;
}

//////////////////////////////////////////////////////////////////////////////
//  Load Image File based on its full path and name
//////////////////////////////////////////////////////////////////////////////
bool NumpadDlg::LoadFile(LPWSTR pszNewFilename)
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	WCHAR szFileName[MAX_STRING_LENGTH] = L"";
	OPENFILENAME of;

	if (!pszNewFilename)
	{
		pszNewFilename = (LPWSTR)GlobalAlloc(GPTR, 261 * sizeof(WCHAR));
		if (!pszNewFilename)
			return false;
	}

	ZeroMemory(&of, sizeof(of));

	of.lStructSize = sizeof(of);
	of.hwndOwner = CDialogEx::CDialog::CWnd::m_hWnd;//m_hwndDialog;
	of.lpstrFilter = L"PNG Files(*.png)\0*.PNG"
		             L"\0BMP Files(.bmp)\0*.bmp"
		             L"\0JPG Files(.jpg)\0*.jpg"
		             L"\0JPEG Files(.jpeg)\0*.jpeg"
		             L"\0GIF Files(.gif)\0*.gif";
					 //L"\0All Files(*.*)\0*.*\0";
	of.lpstrFile = szFileName;
	of.nMaxFile = MAX_STRING_LENGTH;
	of.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	of.lpstrDefExt = L"PNG";

	BOOL temp = GetOpenFileName(&of);
	if (temp)
	{
		HANDLE hFile;
		hFile = CreateFile(of.lpstrFile, GENERIC_READ, NULL, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

		if (hFile == INVALID_HANDLE_VALUE)
		{
			wsprintf(outstring, L"File \"%ws\" does not exist!", of.lpstrFile);
			Log(outstring);
			CloseHandle(hFile);
			return(false);
		}
		if (hFile != INVALID_HANDLE_VALUE)
		{
			DWORD dwFileSize;
			dwFileSize = GetFileSize(hFile, NULL);

			if(dwFileSize > 0)
			{
				wcscpy_s(pszNewFilename, MAX_STRING_LENGTH,  of.lpstrFile);
			}
			CloseHandle(hFile);
		}
	}
	return (0 != temp);
}

//////////////////////////////////////////////////////////////////////////////
//  Render DK Image for DK UI
//////////////////////////////////////////////////////////////////////////////
void NumpadDlg::DrawDKImage(RZDKSTATETYPE dkState, LPCWSTR pszwFilename)
{
	HRESULT hr = S_OK; 
	CImage imageFile; 
    HDC hDC;
	HDC hSurfaceDC = ::GetDC(m_hwndDialog);
    HDC hScreenDC = CreateCompatibleDC(hSurfaceDC);
	RECT rc;
	if (dkState == RZSDKSTATE_UP)
		memcpy(&rc, &m_Images.m_RectUp, sizeof(RECT));
	else
		memcpy(&rc, &m_Images.m_RectDown, sizeof(RECT));

	hr = imageFile.Load(pszwFilename);

	CRect rect(CPoint(0, 0), CSize(SWITCHBLADE_DYNAMIC_KEY_X_SIZE, SWITCHBLADE_DYNAMIC_KEY_Y_SIZE));

    if (hr != S_OK) return; 

    hDC = imageFile.GetDC(); 

    HDC hMemDC = CreateCompatibleDC(hDC);
    if(hMemDC)
    {
        void* pBits = NULL;

        BITMAPV5HEADER bmi = {0};
        bmi.bV5Size        = sizeof(BITMAPV5HEADER);

        bmi.bV5Width       = rect.Width();
        bmi.bV5Height      = rect.Height();

        bmi.bV5SizeImage   = bmi.bV5Width * bmi.bV5Height * 2;
        bmi.bV5CSType      = LCS_WINDOWS_COLOR_SPACE;
        bmi.bV5Intent      = LCS_GM_IMAGES;
        bmi.bV5BitCount    = 16;
        bmi.bV5Planes      = 1;
        bmi.bV5Compression = BI_BITFIELDS;
        bmi.bV5RedMask     = 0x0000F800;   // 5
        bmi.bV5GreenMask   = 0x000007E0;   // 6
        bmi.bV5BlueMask    = 0x0000001F;   // 5

        HBITMAP hBitMap = CreateDIBSection(hDC, (BITMAPINFO*)&bmi, DIB_RGB_COLORS, &pBits, 0, 0);
        if(hBitMap)
        {
            HBITMAP hOldBitMap = (HBITMAP)SelectObject(hMemDC, hBitMap);
			BitBlt(hSurfaceDC, rc.left, rc.top, bmi.bV5Width, bmi.bV5Height, hDC, 0, 0, SRCCOPY);
			SelectObject(hMemDC, hOldBitMap);
            DeleteObject(hBitMap);
        }
        DeleteDC(hMemDC);
        DeleteDC(hScreenDC);
        DeleteDC(hSurfaceDC);
    }

    imageFile.ReleaseDC(); 
}

//////////////////////////////////////////////////////////////////////////////
//  Click Load Image Buttton to load the iamge file
//////////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnBnClickedLoadImage(UINT nID)
{
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	WCHAR szScratchString[MAX_STRING_LENGTH] = L"";
	RZDKSTATETYPE dkState = (nID == IDC_BUTTON_LOAD_UP) ? RZSDKSTATE_UP : RZSDKSTATE_DOWN;
	LPWSTR pDest = (nID == IDC_BUTTON_LOAD_UP)
		          ? m_Images.m_ImagefilenameCurrrentUp
		 		  : m_Images.m_ImagefilenameCurrrentDown;

	if (m_Images.LastDKPressed == RZDYNAMICKEY_INVALID)
	{
		wsprintf(outstring, L"No dynamic key was selected. Select a dynamic key, then set the image(s).");
		Log(outstring);
		return;
	}

	// open a browse control to navigate to the file
	bool bResult = LoadFile(szScratchString);
	if (!bResult)
		return;

	wcscpy_s(pDest, MAX_STRING_LENGTH, szScratchString);
	DrawDKImage(dkState, pDest);

	wsprintf(outstring, L"%ws loaded to current %ws image", pDest, RZDKSTATEToString(dkState));
	Log(outstring);
}

///////////////////////////////////////////////////////////////////////////////////////
//  Translate the regions in Windows UI Space to the region in  SwitchBlade LCD
///////////////////////////////////////////////////////////////////////////////////////
void NumpadDlg::GetStaticRegions()
{
	POINT dlgTopLeft = {0, 0};
	::GetWindowRect(this->m_Images.m_CurrentUp, &m_Images.m_RectUp);
	::GetWindowRect(this->m_Images.m_CurrentDown, &m_Images.m_RectDown);
	::GetWindowRect(this->m_Images.m_CurrentBackground, &m_Images.m_RectBackground);

	// Translate coordinates to more useful coordinates: those that
	// are used on the dialog.
	// In order to do the translation we have to find the top left
	// point (coordinates) of the dialog's client:
	::ClientToScreen(this->m_hWnd, &dlgTopLeft);

	// With these coordinates we can do the translation.
	m_Images.m_RectUp.top    -= dlgTopLeft.y;
	m_Images.m_RectUp.left   -= dlgTopLeft.x;
	m_Images.m_RectUp.bottom = m_Images.m_RectUp.top + SWITCHBLADE_DYNAMIC_KEY_Y_SIZE;
	m_Images.m_RectUp.right  = m_Images.m_RectUp.left + SWITCHBLADE_DYNAMIC_KEY_X_SIZE;

	m_Images.m_RectDown.top    -= dlgTopLeft.y;
	m_Images.m_RectDown.left   -= dlgTopLeft.x;
	m_Images.m_RectDown.bottom = m_Images.m_RectDown.top + SWITCHBLADE_DYNAMIC_KEY_Y_SIZE;
	m_Images.m_RectDown.right  = m_Images.m_RectDown.left + SWITCHBLADE_DYNAMIC_KEY_X_SIZE;

	m_Images.m_RectBackground.top    -= dlgTopLeft.y;
	m_Images.m_RectBackground.left   -= dlgTopLeft.x;
	m_Images.m_RectBackground.bottom = m_Images.m_RectBackground.top + SWITCHBLADE_TOUCHPAD_Y_SIZE;
	m_Images.m_RectBackground.right  = m_Images.m_RectBackground.left + SWITCHBLADE_TOUCHPAD_X_SIZE;
}


//////////////////////////////////////////////////////////////////////////////////
// Checks the filename for one of the accepted file types. we're case insensitive
// since we lowercase everything for comparison
/////////////////////////////////////////////////////////////////////////////////
FILETYPE MyGetFileType(LPWSTR lpszImageFilename)
{
	errno_t tmp = 0;
	FILETYPE ftReturn = ft_invalid;
	size_t nLength = wcslen(lpszImageFilename)+1;
	//
	// secure tolower(). We can do this because the leaf routines all copy the strings to
	// a preallocated buffer.
	//
	tmp = _wcslwr_s(lpszImageFilename, nLength);

	if (wcsstr(lpszImageFilename, L"png"))
		ftReturn = ft_png;
	else if ((wcsstr(lpszImageFilename, L"jpg"))
		 ||  (wcsstr(lpszImageFilename, L"jpeg")))
		ftReturn = ft_jpg;
	else if (wcsstr(lpszImageFilename, L"bmp"))
		ftReturn = ft_bmp;
	else if (wcsstr(lpszImageFilename, L"gif"))
		ftReturn = ft_gif;

	return ftReturn;
}


///////////////////////////////////////////////////////////////////////
// if the updated filename is different than that already in use,
// use the updated filename. Also report whether an update took
// place so that we can know whether to process further based on
// the revised filename
///////////////////////////////////////////////////////////////////////
bool NumpadDlg::UpdateFileStrings(LPWSTR pszNew, LPWSTR pszOld)
{
	bool bResult = false;
	size_t length = 0;

	// check the image string for the DK. If null, just assign
	length = wcslen(pszOld);
	if (0 == length)
	{
		wcscpy_s(pszOld, MAX_STRING_LENGTH, pszNew);
		bResult = true;
	}
	else
	{
		int i = 0;
		// update the key file name if there's a change
		i = wcscmp(pszNew, pszOld);
		if (0 != i)
		{
			wcscpy_s(pszOld, MAX_STRING_LENGTH, pszNew);
			bResult = true;
		}
	}

	return bResult;
}


////////////////////////////////////////////////////////////////////////////////////
// Perform the dynamic key update. we have a file different from the original,
// so send it through the SDK, and report how it worked out for us...
/////////////////////////////////////////////////////////////////////////////////////
void NumpadDlg::UpdateDynamicKeyImage(RZDKTYPE dk, RZDKSTATETYPE dkState, LPWSTR filename)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBSetImageDynamicKey(dk, dkState, filename);
	if (RZSB_FAILED(hReturn))
	{
		HRESULT hRes = GetLastError();
		wsprintf(outstring,
			L"DynamicKeySetImage(%ws, %ws, %ws) = 0x%08lx, additional status = 0x%08lx",
			DKTYPEToString(dk),
			RZDKSTATEToString(dkState),
			filename,
			hReturn,
			hRes);
	}
	else
	{
		wsprintf(outstring,
			L"DynamicKeySetImage(%ws, %ws, %ws) = RZSB_OK",
			DKTYPEToString(dk),
			RZDKSTATEToString(dkState),
			filename);
	}

	Log(outstring);
	return;
}

/////////////////////////////////////////////////////////////////////////////////
// Click "Update" Button, presumably after updating one or both of the current
// images. verify the user's choices and update if it makes sense to do so.
/////////////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnClickedUpdateButton()
{
	bool bUpdateUp = false, bUpdateDown = false;
	size_t UpStringLength = 0, DownStringLength = 0;
	RZDKTYPE dk = m_Images.LastDKPressed;

	// Update the current DK with the images currently displayed
	if (!ValidDynamicKey(dk))
		return;

	// collect the lengths of the update strings, because we use them in more than one place...
	UpStringLength = wcslen(m_Images.m_ImagefilenameCurrrentUp);
	DownStringLength = wcslen(m_Images.m_ImagefilenameCurrrentDown);

	// if both the update strings are null, there's nothing to do...
	if ((0 == UpStringLength) && (0 == DownStringLength))
		return;

	// only update if the new string is not NULL.
	if (0 != UpStringLength)
		bUpdateUp = UpdateFileStrings(m_Images.m_ImagefilenameCurrrentUp, m_Images.m_ImageFilenamesUp[dk]);

	// valid update (key not null) -- send to the DK
	UpdateDynamicKeyImage(dk, RZSDKSTATE_UP, m_Images.m_ImageFilenamesUp[dk]);

	// only update if the new string is not NULL.
	if (0 != DownStringLength)
		bUpdateDown = UpdateFileStrings(m_Images.m_ImagefilenameCurrrentDown, m_Images.m_ImageFilenamesDown[dk]);

	// valid update (key not null) -- send to the DK
	UpdateDynamicKeyImage(dk, RZSDKSTATE_DOWN, m_Images.m_ImageFilenamesDown[dk]);

	DrawDKImage(RZSDKSTATE_UP, m_Images.m_ImageFilenamesUp[dk]);
	DrawDKImage(RZSDKSTATE_DOWN, m_Images.m_ImageFilenamesDown[dk]);
}

/////////////////////////////////////////////////////////////////////////
// Converting  a gesture type to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
RZSDKGESTURETYPEToString(
	RZSDKGESTURETYPE gesture
	)
{
	if (!ValidGesture(gesture))
		return L"Gesture out of range";
	if (!SingleGesture(gesture))
		return L"Compound gesture";

	if (gesture == RZGESTURE_INVALID)
		return L"Invalid";
	else if (gesture == RZGESTURE_NONE)
		return L"None";
	else if (gesture == RZGESTURE_PRESS)
		return L"Press";
	else if (gesture == RZGESTURE_TAP)
		return L"Tap";
	else if (gesture == RZGESTURE_FLICK)
		return L"Flick";
	else if (gesture == RZGESTURE_ZOOM)
		return L"Zoom";
	else if (gesture == RZGESTURE_ROTATE)
		return L"Rotate";
	else if (gesture == RZGESTURE_ALL)
		return L"All";
	else if (gesture == RZGESTURE_UNDEFINED)
		return L"Undefined";

	return L"Completely Bogus Gesture!";
}

/////////////////////////////////////////////////////////////////////////
// Converting the type of dynamic key to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
DKTYPEToString(
	RZDKTYPE dk
	)
{
	if (!ValidDynamicKey(dk))
		return L"DK value out of range";

	switch (dk) {
	case RZDYNAMICKEY_INVALID:
		return L"invalid dynamic key";
    case RZDYNAMICKEY_DK1:
		return L"DK 1";
    case RZDYNAMICKEY_DK2:
		return L"DK 2";
    case RZDYNAMICKEY_DK3:
		return L"DK 3";
    case RZDYNAMICKEY_DK4:
		return L"DK 4";
    case RZDYNAMICKEY_DK5:
		return L"DK 5";
    case RZDYNAMICKEY_DK6:
		return L"DK 6";
    case RZDYNAMICKEY_DK7:
		return L"DK 7";
    case RZDYNAMICKEY_DK8:
		return L"DK 8";
    case RZDYNAMICKEY_DK9:
		return L"DK 9";
    case RZDYNAMICKEY_DK10:
		return L"DK 10";
	case RZDYNAMICKEY_UNDEFINED:
	default:
		return L"undefined dynamic key";
	}
//	return "DKTYPEToString error"; // 4702 compiler warning (unreachable code)
}

/////////////////////////////////////////////////////////////////////////
// Converting the state of dynamic key to a string
/////////////////////////////////////////////////////////////////////////
LPCWSTR
__inline
RZDKSTATEToString(
	RZDKSTATETYPE dkState
	)
{
	if (!ValidDynamicKeyState(dkState))
		return L"DK state out of range";

	switch (dkState) {
    case RZSDKSTATE_INVALID:
		return L"invalid dynamic key state";
    case RZSDKSTATE_DOWN:
		return L"Down";
    case RZSDKSTATE_UP:
		return L"Up";
    case RZSDKSTATE_UNDEFINED:
	default:
		return L"undefined dynamic key state";
	}
//	return "RZDKSTATEToString error"; // 4702 compiler warning (unreachable code)
}

/////////////////////////////////////////////////////////////////////////
// Enable/disable the callback of the gesture control for Application
/////////////////////////////////////////////////////////////////////////
void
NumpadDlg::SetGestureNotify(
	RZSDKGESTURETYPE Gesture,
	bool bEnable
	)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBGestureSetNotification(Gesture, bEnable);
	if (RZSB_SUCCESS(hReturn))
	{
		wsprintf(outstring,
			L"RzSBGestureSetNotification(%ws, %ws) = RZSB_OK",
			RZSDKGESTURETYPEToString(Gesture),
			(bEnable) ? L"True" : L"False");
	}
	else
	{
		wsprintf(outstring,
			L"RzSBGestureSetNotification(%ws, %ws) = 0x%08lx, additional status = 0x%08lx",
			RZSDKGESTURETYPEToString(Gesture),
			(bEnable) ? L"True" : L"False",
			hReturn,
			GetLastError());
	}
	Log(outstring);
}

/////////////////////////////////////////////////////////////////////////
// Enable/disable the callback of the gesture control for OS
/////////////////////////////////////////////////////////////////////////
void
NumpadDlg::SetOSGestureForwarding(
	RZSDKGESTURETYPE Gesture,
	bool bEnable
	)
{
	HRESULT hReturn = S_OK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";

	hReturn = RzSBGestureSetOSNotification(Gesture, bEnable);
	if (RZSB_SUCCESS(hReturn))
	{
		wsprintf(outstring,
			L"RzSBGestureSetOSNotification(%ws, %ws) = RZSB_OK",
			RZSDKGESTURETYPEToString(Gesture),
			(bEnable) ? L"True" : L"False");
	}
	else
	{
		wsprintf(outstring,
			L"RzSBGestureSetOSNotification(%ws, %ws) = 0x%08lx, additional status = 0x%08lx",
			RZSDKGESTURETYPEToString(Gesture),
			(bEnable) ? L"True" : L"False",
			hReturn,
			GetLastError());
	}
	Log(outstring);
}

/////////////////////////////////////////////////////////////////////////
// Select Gesture Type--Press, enable/disable the gesture press callback
// for application or OS
/////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnCbnSelchangeComboPress()
{
	RZSDKGESTURETYPE gesture = RZGESTURE_PRESS;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int nSelection = -1; // default (invalid). valid values are 0, 1 and 2...

	// A change was made to the gesture control for Press

	nSelection = m_PressCombo.GetCurSel();
	if (-1 == nSelection)
	{
		wsprintf(outstring,	L"OnCbnSelchangeComboPress, GetCurSel returns %d ", nSelection);
		Log(outstring);
		return;
	}
	//
	// getCurSel() : 0 == enable notify (app gets gesture)
	//				 1 == disable notify (app doesn't get gesture)
	//               2 == enable OS forwarding (OS gets gesture)
	//               3 == disable OS forwarding (OS doesn't get gesture)
	//               anything else is bogus
	switch (nSelection) {
	case 0:
		SetGestureNotify(gesture, true);
		break;
	case 1:
		SetGestureNotify(gesture, false);
		break;
	case 2:
		SetOSGestureForwarding(gesture, true);
		break;
	case 3:
		SetOSGestureForwarding(gesture, false);
		break;
	default:
		break;
	}
	return;
}

/////////////////////////////////////////////////////////////////////////
// Select Gesture Type--Tap, enable/disable the gesture tap callback
// for application or OS
/////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnCbnSelchangeComboTap()
{
	RZSDKGESTURETYPE gesture = RZGESTURE_TAP;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int nSelection = -1; // default (invalid). valid values are 0, 1 and 2...

	// A change was made to the gesture control for Press

	nSelection = m_TapCombo.GetCurSel();
	if (-1 == nSelection)
	{
		wsprintf(outstring,	L"OnCbnSelchangeComboTap, GetCurSel returns %d ", nSelection);
		Log(outstring);
		return;
	}
	//
	// getCurSel() : 0 == enable notify (app gets gesture)
	//				 1 == disable notify (app doesn't get gesture)
	//               2 == enable OS forwarding (OS gets gesture)
	//               3 == disable OS forwarding (OS doesn't get gesture)
	//               anything else is bogus
	switch (nSelection) {
	case 0:
		SetGestureNotify(gesture, true);
		break;
	case 1:
		SetGestureNotify(gesture, false);
		break;
	case 2:
		SetOSGestureForwarding(gesture, true);
		break;
	case 3:
		SetOSGestureForwarding(gesture, false);
		break;
	default:
		break;
	}
	return;
}

/////////////////////////////////////////////////////////////////////////
// Select Gesture Type--Flick, enable/disable the gesture flick callback
// for application or OS
/////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnCbnSelchangeComboFlick()
{
	RZSDKGESTURETYPE gesture = RZGESTURE_FLICK;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int nSelection = -1; // default (invalid). valid values are 0, 1 and 2...

	// A change was made to the gesture control for Press

	nSelection = m_FlickCombo.GetCurSel();
	if (-1 == nSelection)
	{
		wsprintf(outstring,	L"OnCbnSelchangeComboFlick, GetCurSel returns %d ", nSelection);
		Log(outstring);
		return;
	}
	//
	// getCurSel() : 0 == enable notify (app gets gesture)
	//				 1 == disable notify (app doesn't get gesture)
	//               2 == enable OS forwarding (OS gets gesture)
	//               3 == disable OS forwarding (OS doesn't get gesture)
	//               anything else is bogus
	switch (nSelection) {
	case 0:
		SetGestureNotify(gesture, true);
		break;
	case 1:
		SetGestureNotify(gesture, false);
		break;
	case 2:
		SetOSGestureForwarding(gesture, true);
		break;
	case 3:
		SetOSGestureForwarding(gesture, false);
		break;
	default:
		break;
	}
	return;
}

/////////////////////////////////////////////////////////////////////////
// Select Gesture Type--Zoom, enable/disable the gesture zoom callback
// for application or OS
/////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnCbnSelchangeComboZoom()
{
	RZSDKGESTURETYPE gesture = RZGESTURE_ZOOM;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int nSelection = -1; // default (invalid). valid values are 0, 1 and 2...

	// A change was made to the gesture control for Press

	nSelection = m_ZoomCombo.GetCurSel();
	if (-1 == nSelection)
	{
		wsprintf(outstring,	L"OnCbnSelchangeComboZoom, GetCurSel returns %d ", nSelection);
		Log(outstring);
		return;
	}
	//
	// getCurSel() : 0 == enable notify (app gets gesture)
	//				 1 == disable notify (app doesn't get gesture)
	//               2 == enable OS forwarding (OS gets gesture)
	//               3 == disable OS forwarding (OS doesn't get gesture)
	//               anything else is bogus
	switch (nSelection) {
	case 0:
		SetGestureNotify(gesture, true);
		break;
	case 1:
		SetGestureNotify(gesture, false);
		break;
	case 2:
		SetOSGestureForwarding(gesture, true);
		break;
	case 3:
		SetOSGestureForwarding(gesture, false);
		break;
	default:
		break;
	}
	return;
}

/////////////////////////////////////////////////////////////////////////
// Select Gesture Type--Rotate, enable/disable the gesture rotate callback
// for application or OS
/////////////////////////////////////////////////////////////////////////
void NumpadDlg::OnCbnSelchangeComboRotate()
{
	RZSDKGESTURETYPE gesture = RZGESTURE_ROTATE;
	WCHAR outstring[MAX_STRING_LENGTH] = L"";
	int nSelection = -1; // default (invalid). valid values are 0, 1 and 2...

	// A change was made to the gesture control for Press

	nSelection = m_RotateCombo.GetCurSel();
	if (-1 == nSelection)
	{
		wsprintf(outstring,	L"OnCbnSelchangeComboRotate, GetCurSel returns %d ", nSelection);
		Log(outstring);
		return;
	}
	//
	// getCurSel() : 0 == enable notify (app gets gesture)
	//				 1 == disable notify (app doesn't get gesture)
	//               2 == enable OS forwarding (OS gets gesture)
	//               3 == disable OS forwarding (OS doesn't get gesture)
	//               anything else is a bug
	switch (nSelection) {
	case 0:
		SetGestureNotify(gesture, true);
		break;
	case 1:
		SetGestureNotify(gesture, false);
		break;
	case 2:
		SetOSGestureForwarding(gesture, true);
		break;
	case 3:
		SetOSGestureForwarding(gesture, false);
		break;
	default:
		break;
	}
	return;
}
