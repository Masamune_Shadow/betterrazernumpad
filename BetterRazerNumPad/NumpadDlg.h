
// TestDlg.h : header file
//

#pragma once
#pragma warning(push, 1)
#include "afxwin.h"
#pragma warning(pop)
#include <math.h>
#include "SwitchBlade.h"		// SwitchBlade SDK support

/******************************************************************
*                                                                 *
*  Macros                                                         *
*                                                                 *
******************************************************************/

template<class Interface>
inline void
SafeRelease(
    Interface **ppInterfaceToRelease
    )
{
    if (*ppInterfaceToRelease != NULL)
    {
        (*ppInterfaceToRelease)->Release();

        (*ppInterfaceToRelease) = NULL;
    }
}

#ifndef Assert
#if defined( DEBUG ) || defined( _DEBUG )
#define Assert(b) do {if (!(b)) {OutputDebugStringA("Assert: " #b "\n");}} while(0)
#else
#define Assert(b)
#endif //DEBUG || _DEBUG
#endif

#ifndef HINST_THISCOMPONENT
EXTERN_C IMAGE_DOS_HEADER __ImageBase;
#define HINST_THISCOMPONENT ((HINSTANCE)&__ImageBase)
#endif

typedef HRESULT (STDMETHODCALLTYPE AppEvent)(RZSDKAPPEVENTTYPE, DWORD, DWORD);
typedef HRESULT (STDMETHODCALLTYPE DKEvent)(RZDKTYPE, RZDKSTATETYPE);
typedef HRESULT (STDMETHODCALLTYPE GestureEvent)(RZSDKGESTURETYPE,DWORD,WORD,WORD,WORD);

typedef struct _BM_
{
	bool set;
	CBitmap bm;
} BM, *PBM;


class CImages : public CDialogEx
{
public:
	// coords on dialog of these elements
	RECT m_RectUp, m_RectDown, m_RectBackground;

	// default image to use for a DK when there is none available
	BM m_DKDefault;

	RZDKTYPE LastDKPressed;

	// filename associated with the images currently displayed on the statics
	WCHAR m_ImagefilenameCurrrentUp[MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameCurrrentDown[MAX_STRING_LENGTH];

	// filenames associated with the set images for the DKs
	WCHAR m_ImageFilenamesUp[RZDYNAMICKEY_UNDEFINED][MAX_STRING_LENGTH];
	WCHAR m_ImageFilenamesDown[RZDYNAMICKEY_UNDEFINED][MAX_STRING_LENGTH];
	WCHAR m_ImagefilenameBackground[MAX_STRING_LENGTH];

	// images currently displayed
	CStatic m_CurrentUp;
	CStatic m_CurrentDown;
	CStatic m_CurrentBackground;

	// the undisplayed (but cached) images we have for the dynamic keys
	BM m_DKImagesUp[RZDYNAMICKEY_UNDEFINED];
	BM m_DKImagesDown[RZDYNAMICKEY_UNDEFINED];
};

// NumpadDlg dialog
class NumpadDlg : public CDialogEx
{
// Construction
public:
	NumpadDlg(CWnd* pParent = NULL);	// standard constructor
	HWND       m_hwndDialog;
	HWND       m_hOutput;
	HINSTANCE  m_ghInstance;
	HRESULT    m_RzSBStartStatus;	// status from RzSBStart(), called in FunctionTest.cpp
// Dialog Data
	enum { IDD = IDD_TEST_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	void InitUIDefaults(void);
	void InitStructs(void);
	void LoadFileDefaults(void);
	bool UpdateFileStrings(LPWSTR, LPWSTR);
	void UpdateDynamicKeyImage(RZDKTYPE, RZDKSTATETYPE, LPWSTR);
	void SetGestureNotify(RZSDKGESTURETYPE, bool);
	void SetOSGestureForwarding(RZSDKGESTURETYPE, bool);
	void GetStaticRegions(void);

public:
	HRESULT ApplicationCallback(RZSDKAPPEVENTTYPE, DWORD, DWORD);
	HRESULT DynamicKeyCallback(RZDKTYPE, RZDKSTATETYPE);
	HRESULT TouchpadGestureCallback(RZSDKGESTURETYPE, DWORD, WORD, WORD, WORD);

	afx_msg void OnClickedUpdateButton();

	CComboBox m_PressCombo;
	CComboBox m_TapCombo;
	CComboBox m_FlickCombo;
	CComboBox m_ZoomCombo;
	CComboBox m_RotateCombo;

	afx_msg void OnCbnSelchangeComboPress();
	afx_msg void OnCbnSelchangeComboTap();
	afx_msg void OnCbnSelchangeComboFlick();
	afx_msg void OnCbnSelchangeComboZoom();
	afx_msg void OnCbnSelchangeComboRotate();

	static HRESULT ApplicationEventCallback(RZSDKAPPEVENTTYPE, DWORD, DWORD);
	static HRESULT DynamicKeyEventCallback(RZDKTYPE, RZDKSTATETYPE);
	static HRESULT GestureEventCallback(RZSDKGESTURETYPE,DWORD,WORD,WORD,WORD);

	afx_msg void OnBnClickedButton(UINT);
	afx_msg void OnBnClickedLoadImage(UINT);
	SHORT m_ImageSizes[2];
	LPBYTE m_ImageData[2];
private:
	bool LoadFile(LPWSTR);
	void DrawDKImage(RZDKSTATETYPE, LPCWSTR);
	CImages m_Images;
};