//// PopupDialog.cpp : implementation file
////
//
//#include "stdafx.h"
//#include "BetterRazerNumPad.h"
//#include "PopupDlg.h"
//#include "afxdialogex.h"
//#include "SwitchBlade.h"
//
//extern CString g_csImageFilename;
//
//// CPopupDialog dialog
//
//HRESULT STDMETHODCALLTYPE MyKeyboardEventCallback(UINT uMsg, WPARAM wParam, LPARAM lParam);
//
//IMPLEMENT_DYNAMIC(CPopupDialog, CDialog)
//
//CPopupDialog::CPopupDialog(CWnd* pParent /*=NULL*/)
//	: CDialog(CPopupDialog::IDD, pParent)
//{
//	m_nCSMonitors = ::GetSystemMetrics(SM_CMONITORS);
//	m_nCXScreen = ::GetSystemMetrics(SM_CXSCREEN);
//	m_nCYScreen = ::GetSystemMetrics(SM_CYSCREEN);
//	m_nCXMaximized = ::GetSystemMetrics(SM_CXMAXIMIZED);
//	m_nCYMaximized = ::GetSystemMetrics(SM_CYMAXIMIZED);
//	m_nCXBorder = ::GetSystemMetrics(SM_CXBORDER);
//	m_nCYBorder = ::GetSystemMetrics(SM_CYBORDER);
//	m_nCXVirtualScreen = ::GetSystemMetrics(SM_CXVIRTUALSCREEN);
//	m_nCYVirtualScreen = ::GetSystemMetrics(SM_CYVIRTUALSCREEN);
//	m_nXVirtualScreen = ::GetSystemMetrics(SM_XVIRTUALSCREEN);
//	m_nYVirtualScreen = ::GetSystemMetrics(SM_YVIRTUALSCREEN);
//	m_csLocation = _T("");
//}
//
//CPopupDialog::~CPopupDialog()
//{
//}
//
//void CPopupDialog::DoDataExchange(CDataExchange* pDX)
//{
//	CDialog::DoDataExchange(pDX);
//	DDX_Text(pDX, IDC_CMONITORS, m_nCSMonitors);
//	DDX_Text(pDX, IDC_CXSCREEN, m_nCXScreen);
//	DDX_Text(pDX, IDC_CYSCREEN, m_nCYScreen);
//	DDX_Text(pDX, IDC_CXMAXIMIZED, m_nCXMaximized);
//	DDX_Text(pDX, IDC_CYMAXIMIZED, m_nCYMaximized);
//	DDX_Text(pDX, IDC_CXBORDER, m_nCXBorder);
//	DDX_Text(pDX, IDC_CYBORDER, m_nCYBorder);
//	DDX_Text(pDX, IDC_CXVIRTUALSCREEN, m_nCXVirtualScreen);
//	DDX_Text(pDX, IDC_CYVIRTUALSCREEN, m_nCYVirtualScreen);
//	DDX_Text(pDX, IDC_XVIRTUALSCREEN, m_nXVirtualScreen);
//	DDX_Text(pDX, IDC_YVIRTUALSCREEN, m_nYVirtualScreen);
//	DDX_Text(pDX, IDC_LOCATION, m_csLocation);
//}
//
//BEGIN_MESSAGE_MAP(CPopupDialog, CDialog)
//	ON_BN_CLICKED(IDOK, &CPopupDialog::OnBnClickedOk)
//	ON_BN_CLICKED(IDCANCEL, &CPopupDialog::OnBnClickedCancel)
//END_MESSAGE_MAP()
//
//// CPopupDialog message handlers
//
//void CPopupDialog::OnBnClickedOk()
//{
//	RzSBWinRenderStop(false);
//	CDialog::OnOK();
//}
//
//void CPopupDialog::OnBnClickedCancel()
//{
//	RzSBWinRenderStop(false);
//	CDialog::OnCancel();
//}
//
//BOOL CPopupDialog::OnInitDialog()
//{
//	CDialog::OnInitDialog();
//
//	RzSBWinRenderSetDisabledImage((LPWSTR)g_csImageFilename.GetBuffer());
//	RzSBWinRenderStart(m_hWnd, true, false);
//	g_csImageFilename.ReleaseBuffer();
//
//	RECT rect;
//	GetWindowRect(&rect);
//	m_csLocation.Format(_T("%d,%d"), rect.left, rect.top);
//	UpdateData(FALSE);
//
//	return TRUE;  // return TRUE unless you set the focus to a control
//}
//
