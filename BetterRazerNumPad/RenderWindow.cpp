#include "stdafx.h"
#include "RenderWindow.h"
#include "include\SwitchBlade.h"

DWORD WINAPI RenderThread( LPVOID lpParam ) 
{
	AFX_MANAGE_STATE(AfxGetAppModuleState());

	HRESULT hr = RzSBStart();
	HWND hwnd = (HWND)lpParam;

	void *pBits = NULL;
	CRect rect(CPoint(0, 0), CSize(800, 480));
	BITMAPV5HEADER bmi = {0};

		bmi.bV5Size        = sizeof(BITMAPV5HEADER);
        bmi.bV5Width       = rect.Width();
        bmi.bV5Height      = rect.Height();
        bmi.bV5SizeImage   = bmi.bV5Width * bmi.bV5Height * 2;
        bmi.bV5CSType      = LCS_WINDOWS_COLOR_SPACE;
        bmi.bV5Intent      = LCS_GM_IMAGES;
        bmi.bV5BitCount    = 16;
        bmi.bV5Planes      = 1;
        bmi.bV5Compression = BI_BITFIELDS;
        bmi.bV5RedMask     = 0x0000F800;   // 5
        bmi.bV5GreenMask   = 0x000007E0;   // 6
        bmi.bV5BlueMask    = 0x0000001F;   // 5
		
	while (TRUE)
	{
		SYSTEMTIME st_start, st_finish;
		::GetSystemTime(&st_start);

		RZSB_BUFFERPARAMS bp;
		HDC hDC = ::GetDC(hwnd);
		HDC hMemDC = CreateCompatibleDC(hDC);
		HBITMAP hOldBitMap, hBitMap;

		hBitMap = CreateDIBSection(hDC, (BITMAPINFO*)&bmi, DIB_RGB_COLORS, &pBits, 0, 0);
		if(hBitMap)
		{
			hOldBitMap = (HBITMAP)SelectObject(hMemDC, hBitMap);
			StretchBlt(hMemDC, 0, 0, bmi.bV5Width, bmi.bV5Height, hDC, 0, bmi.bV5Height-1, bmi.bV5Width, -bmi.bV5Height, SRCCOPY); 

			bp.pData = (BYTE *)pBits;
			bp.DataSize = 800 * 480 * 2;
			bp.PixelType = RGB565;
			HRESULT hr = RzSBRenderBuffer(TARGET_DISPLAY_WIDGET, &bp);
			//Sleep(100);

			SelectObject(hMemDC, hOldBitMap);
			DeleteObject(hBitMap);
			DeleteDC(hMemDC);
			DeleteDC(hDC);
		}

		::GetSystemTime(&st_finish);

		DWORD dwMilliseconds = st_finish.wMilliseconds - st_start.wMilliseconds;
		DWORD dwTest = dwMilliseconds;
	}
	return 0;
}

