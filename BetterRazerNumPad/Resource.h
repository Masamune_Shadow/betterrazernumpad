//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by BetterRazerNumPad.rc
//
#define ID_LOAD_UP                      3
#define IDOK3                           4
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_TEST_DIALOG                 102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDB_PNG_BACK                    182
#define IDB_PNG_BACK_DOWN               186
#define IDB_BITMAP1                     187
#define IDB_BITMAP2                     188
#define IDD_SDK_OVERALL_DIALOG          189
#define IDB_BITMAP3                     191
#define IDB_BITMAP4                     192
#define IDB_DEFAULT_DK_BITMAP           192
#define IDB_BACKGROUND_BITMAP           196
#define IDR_MAINFRAME                   200
#define IDC_BUTTON_DK1                  1000
#define IDC_BUTTON_DK2                  1001
#define IDC_BUTTON_DK3                  1002
#define IDC_BUTTON_DK4                  1003
#define IDC_BUTTON_DK5                  1004
#define IDC_BUTTON_DK6                  1005
#define IDC_BUTTON_DK7                  1006
#define IDC_BUTTON_DK8                  1007
#define IDC_BUTTON_DK9                  1008
#define IDC_BUTTON_DK10                 1009
#define ID_UPDATE_KEY                   1010
#define IDC_STATIC_DOWN                 1012
#define IDC_STATIC_UP                   1013
#define IDC_DK_SELECTION                1014
#define IDC_DOWN_DISPLAY                1015
#define IDC_UP_DISPLAY                  1016
#define IDC_TOUCHPAD_BACKGROUND         1017
#define IDC_BUTTON_LOAD_UP              1018
#define IDC_BUTTON_LOAD_DOWN            1019
#define IDC_BUTTON11                    1021
#define IDC_LIST_SDK_OUTPUT             1022
#define IDC_COMBO_PRESS                 1024
#define IDC_DK_UP_IMAGE                 1025
#define IDC_DK_DOWN_IMAGE               1026
#define IDC_BACKGROUND_IMAGE            1027
#define IDC_COMBO_TAP                   1028
#define IDC_COMBO_FLICK                 1029
#define IDC_COMBO_ZOOM                  1030
#define IDC_COMBO_ROTATE                1031
#define ID_MAINFRAME                    58114

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
