CURRENTLY TODO: (Because dumb update)
Duplication of the key strokes needs to be re-implimented -- DK
Duplication of the key strokes needs to be re-implimented -- Trackpad
calc not showing up by default
FreeMove not showing up by default
Alpha Layering
Simple Color Pallet (RGB) 


Hit the Key right next to the RGB key to get the calc and free move images to display properly, the keys work fine, but images don't show up idk why.


FEATURE LIST:

TACTILE KEYS
The bottom row of tactile keys is where the user can toggle on/off the num lock. Doing so will cause the image displayed on the track pad to change, to correspond to the state of the num lock (on/off)
The bottom row of tactile keys is where the user will find the '/' '*' '-' and '+' keys (in that order, left to right).
The first three tactile keys in the top row are blank.
The fourth key in the top row starts the windows calculator app.
The fifth key in the top row labeled "FREE MODE" disables the numpad-associated functions, and allows the trackpad to return to acting like a mouse. pressing the key again will return the numpad to it's regular functionality.

TRACK PAD
The keys are larger than those found on the standard Razer Numberpad app.
As such, the Zero key, and Decimal Key have been moved to the left of the 3x3 number grid.
The enter key is on the right, and I added in a backspace key to the top right of the numpad because I felt it was handy to have.
users do not need to lift their fingers to switch between keys, detecting a difference in the key vs last key pressed (if pressed) will result in changing to that key.
users also do not need to press the number multiple times for multiple same-number inputs. I have mimiced how the keyboard treats pressed keys allowing "downed" keys to be repeated.

You can exit the app at anytime by hitting the Razer home key.

I really like it, and I hope you guys do too.

